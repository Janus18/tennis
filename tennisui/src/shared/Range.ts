export function Range(x: number): Array<number> {
    const xs = new Array<number>(x);
    return Array.from(xs.keys()).map(i => i);
}
