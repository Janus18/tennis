
export function IsDefined<T>(t: T | null | undefined): t is T {
    return t !== null && t !== undefined;
}

export function TryApply<T, Y>(t: T | null | undefined, f: (x: T) => Y, y: Y): Y {
    return IsDefined(t) ? f(t) : y;
}

export function ValueOrDefault<T>(t: T | null | undefined, x: T): T {
    return TryApply(t, x => x, x);
}
