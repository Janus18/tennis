export function GetEnumValues<T extends object>(e: T): (keyof(T))[] {
    return Object.keys(e).map((i) => (e as any)[i]);
}
