import { IAddTournamentInput } from "@/models/IAddTournamentInput";
import { Player } from "@/models/Player";
import { TournamentsInstancesListItem } from "@/models/TournamentsInstancesListItem";
import { TournamentsListItem } from "@/models/TournamentsListItem";
import { OpResult } from "./models/OpResult";
import { IAddPlayerInput } from "@/models/IAddPlayerInput";

export interface IApiService {
    getTournamentsInstances(
        page: number,
        count: number,
        tournamentId?: string,
        gender?: string,
        winnerId?: string
    ): Promise<Array<TournamentsInstancesListItem> | undefined>;
    
    getPlayers(
        page: number,
        count: number,
        gender?: string,
        name?: string
    ): Promise<Array<Player> | undefined>;
    
    getTournaments(
        page: number,
        count: number
    ): Promise<Array<TournamentsListItem> | undefined>;

    createTournament(value: IAddTournamentInput): Promise<OpResult>;

    createPlayer(value: IAddPlayerInput): Promise<OpResult>;

    deleteTournament(id: string): Promise<OpResult>;

    deletePlayer(id: string): Promise<OpResult>;
}
