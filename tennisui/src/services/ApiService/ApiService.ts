import { TournamentsInstancesListItemInput } from "./models/TournamentsInstancesListItemInput";
import { IsDefined } from "@/shared/NullCheck";
import { TournamentsInstancesListItem } from "@/models/TournamentsInstancesListItem";

import { Player } from "@/models/Player";
import { PlayerInput } from "./models/PlayerInput";
import { IApiService } from "./IApiService";
import { TournamentsListItem } from "@/models/TournamentsListItem";
import { TournamentsListItemInput } from "./models/TournamentsListItemInput";
import { IAddTournamentInput } from "@/models/IAddTournamentInput";
import { OpResult } from "./models/OpResult";
import { IAddPlayerInput } from "@/models/IAddPlayerInput";

export class ApiService implements IApiService {
    constructor(
        private baseUrl: string
    ) { }
        
    public async getTournamentsInstances(
        page: number, count: number, tournamentId?: string, gender?: string, winnerId?: string
    ): Promise<Array<TournamentsInstancesListItem> | undefined> {
        const queries = [
            ['page', page.toString(10)],
            ['count', count.toString(10)]
        ];
        if (IsDefined(tournamentId))
            queries.push(['tournamentId', tournamentId]);
        if (IsDefined(gender))
            queries.push(['gender', gender]);
        if (IsDefined(winnerId))
            queries.push(['winnerId', winnerId]);
        const query = queries.map(q => `${q[0]}=${q[1]}`).join('&');
        const result = await fetch(`${this.baseUrl}/tournamentsinstances?${query}`, { method: 'GET' });
        const tournaments = await this.Process<Array<TournamentsInstancesListItemInput>>(result, 'Get Tournaments');
        if (!IsDefined(tournaments))
            return undefined;
        return tournaments.map(t => TournamentsInstancesListItem.FromInput(t));
    }

    public async getPlayers(page: number, count: number, gender?: string, name?: string): Promise<Array<Player> | undefined> {
        const queries = [
            ['page', page.toString(10)],
            ['count', count.toString(10)]
        ];
        if (IsDefined(gender))
            queries.push(['gender', gender]);
        if (IsDefined(name))
            queries.push(['name', name]);
        const query = queries.map(q => `${q[0]}=${q[1]}`).join('&');
        const result = await fetch(`${this.baseUrl}/players?${query}`, { method: 'GET', cache: 'no-store' });
        const players = await this.Process<Array<PlayerInput>>(result, 'Get Players');
        if (!IsDefined(players))
            return undefined;
        return players.map(t => Player.FromInput(t));
    }

    async getTournaments(
        page: number,
        count: number
    ): Promise<Array<TournamentsListItem> | undefined> {
        const queries = [
            ["page", page.toString(10)],
            ["count", count.toString(10)]
        ];
        const query = queries.map(q => `${q[0]}=${q[1]}`).join('&');
        const result = await fetch(`${this.baseUrl}/tournaments?${query}`, { method: 'GET' });
        const tournaments = await this.Process<Array<TournamentsListItemInput>>(result, 'Get Tournaments');
        if (!IsDefined(tournaments))
            return undefined;
        return tournaments.map(t => TournamentsListItem.FromInput(t));
    }

    async createTournament(value: IAddTournamentInput): Promise<OpResult> {
        const form = new FormData();
        form.append('name', value.name);
        form.append('location', value.location);
        form.append('surfaceType', value.surfaceType);
        form.append('singlesDraw', value.singlesDraw);
        form.append('startDateDay', value.startDateDay.toString(10));
        form.append('startDateMonth', value.startDateMonth.toString(10));
        const image = IsDefined(value.image) && value.image.length > 0 ? value.image[0] : undefined;
        if (IsDefined(image)) {
            form.append('image', image, image.name);
        }
        const result = await fetch(`${this.baseUrl}/tournaments`, { method: 'POST', body: form });
        return result.status === 201 ? OpResult.Success() : OpResult.Error(await this.tryGetJson(result));
    }

    async createPlayer(value: IAddPlayerInput): Promise<OpResult> {
        const form = new FormData();
        form.append('name', value.name);
        form.append('gender', value.gender);
        form.append('strength', value.strength.toString(10));
        form.append('speed', value.speed.toString(10));
        form.append('reaction', value.reaction.toString(10));
        const image = IsDefined(value.image) && value.image.length > 0 ? value.image[0] : undefined;
        if (IsDefined(image)) {
            form.append('image', image, image.name);
        }
        const result = await fetch(`${this.baseUrl}/players`, { method: 'POST', body: form });
        return result.status === 201 ? OpResult.Success() : OpResult.Error(await this.tryGetJson(result));
    }

    async deleteTournament(id: string): Promise<OpResult> {
        try {
            const result = await fetch(`${this.baseUrl}/tournaments/${id}`, { method: 'DELETE' });
            return result.status === 204 ? OpResult.Success() : OpResult.Error(await this.tryGetJson(result));   
        } catch (err) {
            console.error(err);
            return OpResult.Error([]);
        }
    }

    async deletePlayer(id: string): Promise<OpResult> {
        try {
            const result = await fetch(`${this.baseUrl}/players/${id}`, { method: 'DELETE' });
            return result.status === 204 ? OpResult.Success() : OpResult.Error(await this.tryGetJson(result));   
        } catch (err) {
            console.error(err);
            return OpResult.Error([]);
        }
    }

    private async Process<T>(response: Response, description: string): Promise<T | undefined> {
        if (response.ok) {
            return await response.json() as T;
        } else {
            const body = await this.tryGetJson(response);
            console.error(`${description} error. Status: ${response.status}. Body: ${body}`);
            return undefined;
        }
    }

    private async tryGetJson(response: Response): Promise<unknown> {
        try {
            return await response.json();
        } catch (err) {
            return undefined;
        }
    }
}
