import { SurfaceType } from "@/models/SurfaceType";

export interface TournamentsListItemInput {
    id: string;
    name: string;
    surfaceType: SurfaceType;
    location: string;
    singlesDraw: number;
    imageUri: string;
}