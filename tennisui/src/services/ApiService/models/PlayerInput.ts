export interface PlayerInput {
    id: string;
    name: string;
    strength: number;
    speed: number;
    reaction: number;
    gender: string;
    imageUri: string;
}
