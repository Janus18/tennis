import { IsDefined } from "@/shared/NullCheck";

export class OpResult {
    constructor(
        public succeed: boolean = false,
        public errors: Array<{ code: string, description: string }> = []
    ) { }

    static Success(): OpResult {
        return new OpResult(true, []);
    }

    static Error(body: any): OpResult {
        const errors = IsDefined(body) && Array.isArray(body) ? body : [];
        return new OpResult(false, errors);
    }

    public HasErrors(): boolean {
        return this.errors.length > 0;
    }

    public ErrorsDescriptions(): Array<string> {
        return this.errors.map(x => x.description).filter(x => IsDefined(x));
    }
}