export interface TournamentsInstancesListItemInput {
    id: string;
    gender: string;
    date: string;
    name: string;
    surfaceType: string;
    location: string;
    singlesDraw: number;
    imageUri: string;
    winner: {
        id: string;
        name: string;
    };
}
