import { SurfaceType } from "./SurfaceType";

export interface IAddTournamentInput {
    name: string;
    location: string;
    surfaceType: SurfaceType;
    singlesDraw: string;
    startDateDay: number;
    startDateMonth: number;
    image: FileList;
}
