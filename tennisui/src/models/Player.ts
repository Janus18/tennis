import { PlayerInput } from "@/services/ApiService/models/PlayerInput";

export class Player {
    constructor(
        public id: string,
        public name: string,
        public strength: number,
        public speed: number,
        public reaction: number,
        public gender: string,
        public imageUri: string
    ) { }

    public static FromInput(player: PlayerInput): Player {
        return new Player(
            player.id,
            player.name,
            player.strength,
            player.speed,
            player.reaction,
            player.gender,
            player.imageUri);
    }
}
