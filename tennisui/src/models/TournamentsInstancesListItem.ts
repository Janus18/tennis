import { TournamentsInstancesListItemInput } from "@/services/ApiService/models/TournamentsInstancesListItemInput";
import { IsDefined } from "@/shared/NullCheck";
import { DateTime } from "luxon";

export class TournamentsInstancesListItem {
    constructor(
        public id: string = '',
        public gender: string = '',
        public date: DateTime = DateTime.utc(),
        public name: string,
        public surfaceType: string,
        public location: string,
        public singlesDraw: number,
        public imageUri: string,
        public winner?: {
            id: string;
            name: string;
        }
    ) { }
    
    static FromInput(input: TournamentsInstancesListItemInput): TournamentsInstancesListItem {
        return new TournamentsInstancesListItem(
            input.id,
            input.gender,
            DateTime.fromISO(input.date),
            input.name,
            input.surfaceType,
            input.location,
            input.singlesDraw,
            input.imageUri,
            input.winner
        );
    }

    public get isFinished() { return IsDefined(this.winner); }
}
