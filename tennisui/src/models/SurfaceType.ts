export enum SurfaceType {
    Acrylic = 'Acrylic',
    ArtificialClay = 'ArtificialClay',
    ArtificialGrass = 'ArtificialGrass',
    Asphalt = 'Asphalt',
    Carpet = 'Carpet',
    Clay = 'Clay',
    Concrete = 'Concrete',
    Grass = 'Grass',
    Other = 'Other',
}

export function DisplaySurfaceType(t: SurfaceType): string {
    switch (t) {
        case SurfaceType.ArtificialClay:
            return "Artificial Clay";
        case SurfaceType.ArtificialGrass:
            return "Artificial Grass";
        default:
            return t;
    }
}