import { TournamentsListItemInput } from "@/services/ApiService/models/TournamentsListItemInput";
import { SurfaceType } from "./SurfaceType";

export class TournamentsListItem {
    constructor(
        public id: string,
        public name: string,
        public surfaceType: SurfaceType,
        public location: string,
        public singlesDraw: number,
        public imageUri: string,
    ) { }
    
    public static FromInput(input: TournamentsListItemInput): TournamentsListItem {
        return new TournamentsListItem(
            input.id,
            input.name,
            input.surfaceType,
            input.location,
            input.singlesDraw,
            input.imageUri
        );
    }
}
