export interface IAddPlayerInput {
    name: string;
    strength: number;
    speed: number;
    reaction: number;
    gender: 'Male' | 'Female';
    image: FileList;
}
