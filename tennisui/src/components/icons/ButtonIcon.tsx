import { ButtonHTMLAttributes } from "react";
import ArrowPathIcon from "./arrowPathIcon";

export default function ButtonIcon(props: ButtonHTMLAttributes<HTMLButtonElement> & { loading?: boolean }) {
    const { className, children, disabled, loading, ...other } = props;
    return (
        <button className={`
            ${className}
            flex
            flex-row
            h-10
            items-center
            justify-center
            rounded-full
            hover:ring-1
            ring-gray-100
            hover:shadow-xl
            w-10
        `} disabled={disabled || loading} {...other}>
            {!loading && children}
            {
                loading &&
                <div className='flex flex-row justify-center'>
                    <ArrowPathIcon className='animate-spin' />
                </div>
            }
        </button>
    )
}
