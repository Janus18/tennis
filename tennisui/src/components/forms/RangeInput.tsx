import { IsDefined, ValueOrDefault } from "@/shared/NullCheck";
import { ChangeEvent, ForwardedRef, InputHTMLAttributes, forwardRef, useState } from "react";

function half(props: InputHTMLAttributes<HTMLInputElement>) {
    const [min, max]: [number, number] = [ValueOrDefault(props.min as number, 0), ValueOrDefault(props.max as number, 100)];
    return (max - min) / 2;
}

export const RangeInput = forwardRef(function F(props: InputHTMLAttributes<HTMLInputElement> & { label: string }, ref?: ForwardedRef<HTMLInputElement>) {
    const [value, setValue] = useState(half(props).toString(10));
    const { className, onChange, type, ...other } = props;
    const changed = (x: ChangeEvent<HTMLInputElement>) => {
        setValue(x.target.value);
        if (IsDefined(onChange))
            onChange(x);
    };
    return (
        <div className={`${className} flex flex-col`}>
            <div className='flex flex-row justify-between'>
                <span className='text-gray-500'>{props.label}</span>
            </div>
            <input
                type='range'
                className={`
                    accent-lime-600
                    border
                    shadow-lg
                    hover:bg-gray-200
                    focus:outline
                    outline-lime-300
                    p-2
                    rounded-md
                    border-lime-700`}
                {...other}
                onChange={(x) => changed(x)}
                ref={ref} />
            <span className='h-5 text-gray-500  text-end'>{value}</span>
        </div>
    );
});
