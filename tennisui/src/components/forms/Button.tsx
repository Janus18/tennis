import { ButtonHTMLAttributes, ForwardedRef, forwardRef } from "react";
import ArrowPathIcon from "../icons/arrowPathIcon";

export const Button = forwardRef(function f(props: ButtonHTMLAttributes<HTMLButtonElement> & { loading?: boolean }, ref: ForwardedRef<HTMLButtonElement>) {
    const { className, children, disabled, loading, ...other } = props;
    return (
        <button className={`
            ${className}
            ${loading ? 'bg-gray-500' : 'bg-lime-600'}
            focus:outline
            ${loading ? '' : 'hover:bg-lime-700'}
            hover:drop-shadow-xl
            p-2
            max-w-xs
            outline-4
            outline-lime-300
            rounded
            shadow-lg
            text-white
        `} disabled={disabled || loading} {...other} ref={ref}>
            {!loading && children}
            {
                loading &&
                <div className='flex flex-row justify-center'>
                    <ArrowPathIcon className='animate-spin' />
                </div>
            }
        </button>
    );
});