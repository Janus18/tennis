import { IsDefined } from "@/shared/NullCheck";
import { ForwardedRef, InputHTMLAttributes, forwardRef } from "react";
import ExclamationCircleIcon from "../icons/exclamationCircleIcon";

export const Input = forwardRef(function f(props: InputHTMLAttributes<HTMLInputElement> & { label: string, error?: string }, ref?: ForwardedRef<HTMLInputElement>) {
    const { className, ...other } = props;
    const hasError = IsDefined(props.error) && props.error.length > 0;
    return (
        <div className={`${className} flex flex-col`}>
            <div className='flex flex-row justify-between'>
                <span className='text-gray-500'>{props.label}</span>
                {hasError && <ExclamationCircleIcon className='text-red-700' />}
            </div>
            <input className={`
                bg-gray-100
                border
                shadow-lg
                hover:bg-gray-200
                focus:outline
                ${hasError ? 'outline-red-300' : 'outline-lime-300'}
                p-2
                rounded-md
                ${hasError ? 'border-red-700' : 'border-lime-700'}`} {...other} ref={ref} />
            <span className='h-5 text-red-700 text-sm'>{props.error}</span>
        </div>
    );
});
