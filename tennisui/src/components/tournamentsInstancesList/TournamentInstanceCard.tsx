import Image, { StaticImageData } from "next/image";
import { TournamentsInstancesListItem } from "@/models/TournamentsInstancesListItem";
import { ValueOrDefault } from "@/shared/NullCheck";
import { Card } from "../Card";
import { ForwardedRef, forwardRef } from "react";
import placeholder from 'public/images/tennis-ball.jpg';

export type TournamentInstanceCardProps = {
    tournament: TournamentsInstancesListItem;
    className?: string;
};

export const TournamentInstanceCard = forwardRef(function TIC(props: TournamentInstanceCardProps, ref: ForwardedRef<HTMLDivElement>) {
    const image = ValueOrDefault<string | StaticImageData>(props.tournament.imageUri, placeholder);
    return (
        <Card className={`flex flex-col md:flex-row max-w-xs md:max-w-screen-lg md:h-60 ${props.className}`} ref={ref}>
            <Image src={image} alt='tournament-logo' className='rounded-t-lg md:rounded-tr-none md:rounded-l-lg h-60 shrink-0 w-full md:w-80' width={320} height={240} />
            <div className='md:ml-8 flex flex-col justify-center p-4 w-full'>
                {
                    props.tournament.isFinished && <p>Finished</p>
                }
                <div className='flex flex-col justify-center items-center md:items-start'>
                    <h5 className='font-bold mb-4 text-2xl text-green-700'>{props.tournament.name}</h5>
                    <p className='font-medium mb-2'>Winner: {props.tournament.winner?.name}</p>
                    <p className='font-medium mb-2'>Location: {props.tournament.location}</p>
                    <p className='font-medium mb-2'>Draw: {props.tournament.singlesDraw}</p>
                </div>
            </div>
        </Card>
    );
});
