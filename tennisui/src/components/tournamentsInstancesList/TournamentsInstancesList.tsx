import { TournamentsInstancesListItem } from "@/models/TournamentsInstancesListItem";
import { TournamentInstanceCard } from "./TournamentInstanceCard";
import { ForwardedRef, forwardRef } from "react";

export type TournamentsInstancesListProps = {
    tournaments: Array<TournamentsInstancesListItem>;
};

export const TournamentsInstancesList = forwardRef(function TIL(props: TournamentsInstancesListProps, ref: ForwardedRef<HTMLDivElement>) {
    const isTrigger = (i: number) => i === props.tournaments.length - 3;
    const isLast = (i: number) => i === props.tournaments.length - 1;
    return (
        <div className="flex flex-col items-center justify-center w-full">
            {props.tournaments.map((t, i) => <TournamentInstanceCard
                key={t.id}
                className={!isLast(i) ? 'mb-4' : ''}
                ref={isTrigger(i) ? ref : undefined}
                tournament={t} />)}
        </div>
    );
});
