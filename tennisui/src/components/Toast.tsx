import { IsDefined } from "@/shared/NullCheck";

export type ToastType = 'info' | 'warn' | 'error';

export default function Toast(props: { type: ToastType, messages: Array<string>, onClose: () => void }) {
    return (
        <div className={`
                ${color(props.type)}
                bottom-3
                shadow-2xl
                fixed
                flex
                flex-row
                items-start
                justify-between
                max-w-lg
                py-2
                px-8
                rounded-md
                text-white
                w-full`
            } onClick={props.onClose}>
            {
                IsDefined(props.messages) && props.messages.length > 1 &&
                <ul className='list-disc w-full'>
                    {props.messages.map((m, i) => <li key={i}>{m}</li>)}
                </ul>
            }
            {
                IsDefined(props.messages) && props.messages.length === 1 && props.messages[0]
            }
            <button className=''>x</button>
        </div>
    )
}

function color(toastType: ToastType): string {
    switch (toastType) {
        case 'error':
            return 'bg-red-700';
        case 'warn':
            return 'bg-amber-600';
        default:
            return 'bg-lime-600';
    }
}