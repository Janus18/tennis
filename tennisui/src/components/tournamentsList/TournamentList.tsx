import { TournamentsListItem } from "@/models/TournamentsListItem";
import { TournamentCard } from "./TournamentCard";
import { ForwardedRef, forwardRef } from "react";

export type TournamentListProps = {
  tournaments: Array<TournamentsListItem>;
  onDelete: (t: TournamentsListItem) => Promise<any>;
  className?: string;
};

export const TournamentList = forwardRef(function TL(props: TournamentListProps, ref: ForwardedRef<HTMLDivElement>) {
  const isTrigger = (i: number) => i === props.tournaments.length - 3;
  const isLast = (i: number) => i === props.tournaments.length - 1;
  return (
    <div className={`flex flex-col items-center justify-center w-full ${props.className}`}>
      {props.tournaments.map((t, i) =>
        <TournamentCard
          className={`${!isLast(i) ? 'mb-3 md:mb-8' : ''}`}
          key={t.id}
          onDelete={() => props.onDelete(t)}
          ref={isTrigger(i) ? ref : undefined}
          tournament={t} />)
      }
    </div>
  );
});
