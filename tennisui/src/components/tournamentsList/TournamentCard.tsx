import { TournamentsListItem } from "@/models/TournamentsListItem";
import Image, { StaticImageData } from "next/image";
import { Card } from "../Card";
import placeholder from 'public/images/tennis-ball.jpg';
import { ValueOrDefault } from "@/shared/NullCheck";
import { ForwardedRef, forwardRef, useState } from "react";
import { DisplaySurfaceType } from "@/models/SurfaceType";
import TrashIcon from "../icons/trashIcon";
import ButtonIcon from "../icons/ButtonIcon";

export type TournamentCardProps = {
    tournament: TournamentsListItem;
    onDelete: () => Promise<any>;
    className?: string;
};

export const TournamentCard = forwardRef(function TC(props: TournamentCardProps, ref: ForwardedRef<HTMLDivElement>) {
    const [deleting, setDeleting] = useState(false);
    const image = ValueOrDefault<string | StaticImageData>(props.tournament.imageUri, placeholder);
    const onDelete = async () => {
        setDeleting(true);
        await props.onDelete();
        setDeleting(false);
    }
    return (
        <Card className={`flex flex-col md:flex-row max-w-xs md:max-w-screen-lg md:h-60 ${props.className}`} ref={ref}>
            <Image src={image} alt='tournament-logo' className='rounded-t-lg md:rounded-tr-none md:rounded-l-lg h-60 shrink-0 w-full md:w-80' width={320} height={240} />
            <div className='h-60 md:ml-8 flex flex-col md:flex-row justify-between p-4 w-full'>
                <div className='flex flex-col justify-center items-center md:items-start'>
                    <h5 className='font-bold mb-4 text-2xl text-green-700'>{props.tournament.name}</h5>
                    <p className='font-medium mb-2'>Location: {props.tournament.location}</p>
                    <p className='font-medium mb-2'>Surface type: {DisplaySurfaceType(props.tournament.surfaceType)}</p>
                    <p className='font-medium mb-2'>Draw: {props.tournament.singlesDraw}</p>
                </div>
                <div className='flex flex-row justify-end items-start'>
                    <ButtonIcon onClick={onDelete} loading={deleting}>
                        <TrashIcon />
                    </ButtonIcon>
                </div>
            </div>
        </Card>
    );
});
