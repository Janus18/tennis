import { ForwardedRef, forwardRef } from "react";

export type CardProps = {
    className?: string;
    children: string | React.ReactNode | Array<string | React.ReactNode>;
};

export const Card = forwardRef(function C(props: CardProps, ref?: ForwardedRef<HTMLDivElement>) {
    return (
        <div className={`${props.className} bg-neutral-100 rounded-lg border shadow-md w-full`} ref={ref}>
            {props.children}
        </div>
    );
});
