export default function Header() {
    const sections = [
        { title: 'Tournaments', path: '/tournaments' },
        { title: 'Players', path: '/players' },
    ];
    return (
        <div className='px-20 h-24 w-full items-center flex flex-row justify-between shadow-lg'>
            <h1 className='font-bold text-2xl'>Tennis</h1>
            <div className='flex flex-row items-center justify-center'>
                {sections.map((s, i) =>
                    <a key={s.path} href={s.path} className='h-full flex flex-column items-center px-4'>
                        <span className='font-thin text-neutral-900 text-sm'>
                            {s.title}
                        </span>
                    </a>
                )}
            </div>
        </div>
    );
}