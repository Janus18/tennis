import { Player } from "@/models/Player";
import { Card } from "../Card";
import Image, { StaticImageData } from "next/image";
import { ValueOrDefault } from "@/shared/NullCheck";
import placeholder from 'public/images/player.jpg';
import { ForwardedRef, forwardRef, useState } from "react";
import ButtonIcon from "../icons/ButtonIcon";
import TrashIcon from "../icons/trashIcon";

export type PlayerCardProps = {
    player: Player;
    onDelete: () => Promise<any>;
    className?: string;
};

export const PlayerCard = forwardRef(function PC(props: PlayerCardProps, ref?: ForwardedRef<HTMLDivElement>) {
    const [deleting, setDeleting] = useState(false);
    const onDelete = async () => {
        setDeleting(true);
        await props.onDelete();
        setDeleting(false);
    }
    const image = ValueOrDefault<string | StaticImageData>(props.player.imageUri, placeholder);
    return (
        <Card className={`flex flex-col md:flex-row max-w-xs md:max-w-screen-lg md:h-60 ${props.className}`} ref={ref}>
            <Image src={image} alt='player-picture' className='rounded-t-lg md:rounded-tr-none md:rounded-l-lg h-60 shrink-0 w-full md:w-80' width={320} height={240} />
            <div className='h-60 md:ml-8 flex flex-col md:flex-row justify-between p-4 w-full'>
                <div className='flex flex-col justify-center items-center md:items-start'>
                    <h5 className='font-bold mb-4 text-2xl text-green-700'>{props.player.name}</h5>
                    <p className='font-medium mb-2'>Reaction: {props.player.reaction}</p>
                    <p className='font-medium mb-2'>Speed: {props.player.speed}</p>
                    <p className='font-medium mb-2'>Strength: {props.player.strength}</p>
                </div>
                <div className='flex flex-row justify-end items-start'>
                    <ButtonIcon onClick={onDelete} loading={deleting}>
                        <TrashIcon />
                    </ButtonIcon>
                </div>
            </div>
        </Card>
    );
});
