import { Player } from "@/models/Player";
import { PlayerCard } from "./PlayerCard";
import { ForwardedRef, forwardRef } from "react";

export type PlayersListProps = {
    players: Array<Player>;
    onDelete: (p: Player) => Promise<any>;
    className?: string;
};

export const PlayersList = forwardRef(function PL(props: PlayersListProps, ref: ForwardedRef<HTMLDivElement>) {
    const isTrigger = (i: number) => i === props.players.length - 3;
    const isLast = (i: number) => i === props.players.length - 1;

    return (
        <div className={`flex flex-col items-center justify-center w-full py-3 md:py-8 ${props.className}`}>
            {props.players.map((p, i) => <PlayerCard
                className={`${!isLast(i) ? 'mb-3 md:mb-8' : ''}`}
                key={p.id}
                onDelete={() => props.onDelete(p)}
                player={p}
                ref={isTrigger(i) ? ref : undefined}
            />)}
        </div>
    );
});
