import { DetailedHTMLProps, FormHTMLAttributes } from "react";

export default function FormCard(props: DetailedHTMLProps<FormHTMLAttributes<HTMLFormElement>, HTMLFormElement>) {
    const { className, ...other } = props;
    return (
        <form className={`
            ${className}
            bg-white
            shadow
            p-4
            md:p-8
            rounded-md
        `} {...other}>
        </form>
    )
}