declare module '*.css';

export declare global {
    namespace JSX {
        interface IntrinsicElements {
            main: DetailedHTMLProps<HTMLAttributes<HTMLElement>, HTMLElement>;
        }
    }
}