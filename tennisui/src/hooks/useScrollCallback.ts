import { useCallback, useMemo } from "react";

export default function useScrollCallback(onApproach: () => unknown) {
    const io = useMemo(() => new IntersectionObserver(
        (entries) => entries[0].isIntersecting ? onApproach() : undefined,
        { root: null, rootMargin: '0px', threshold: 1.0 }
    ), [onApproach]);
    
    const ref = useCallback((node: HTMLDivElement) => {
        if (node === null)
            io?.disconnect();
        else 
            io?.observe(node);
    }, [io]);

    return { ref: ref };
}
