import { IsDefined } from "@/shared/NullCheck";
import { MutableRefObject, useEffect, useState } from "react";
import useScrollCallback from "./useScrollCallback";

export default function useInfiniteScroll<T>(
    pageSize: number,
    componentInitialized: MutableRefObject<boolean>,
    fn: (page: number) => Promise<Array<T> | undefined>,
    addResult: (xs: Array<T>) => void
) {
    const [isLoading, setIsLoading] = useState(false);
    const [disabled, setDisabled] = useState(false);
    const [page, setPage] = useState(0);

    const getList = async () => {
        if (isLoading || disabled)
            return;
        setIsLoading(true);
        const result = await fn(page);
        if (IsDefined(result)) {
            addResult(result);
            setPage(page + 1);
        }
        if (!IsDefined(result) || result.length < pageSize) {
            setDisabled(true);
        }
        setIsLoading(false);
    };

    const { ref } = useScrollCallback(getList);

    useEffect(() => {
        if (componentInitialized.current)
            return;
        componentInitialized.current = true;
        getList();
    }, []);

    return { ref: ref };
}