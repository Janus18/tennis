"use client";
import Toast, { ToastType } from "@/components/Toast";
import { Button } from "@/components/forms/Button";
import { PlayersList } from "@/components/playersList/PlayersList";
import useInfiniteScroll from "@/hooks/useInfiniteScroll";
import { Player } from "@/models/Player";
import { ApiService } from "@/services/ApiService/ApiService";
import { IsDefined, ValueOrDefault } from "@/shared/NullCheck";
import { useSearchParams } from "next/navigation";
import { useMemo, useRef, useState } from "react";

export default function Players() {
    const [players, setPlayers] = useState<Array<Player>>([]);
    const componentInitialized = useRef(false);
    const searchParams = useSearchParams();
    const [toastInfo, setToastInfo] = useState<{ type: ToastType, messages: Array<string> } | undefined>(
        searchParams.has('added') ? { type: 'info', messages: [`Player ${searchParams.get('added')} added`] } : undefined
    );
    
    const apiService = useMemo(() => new ApiService(ValueOrDefault(process.env.NEXT_PUBLIC_API_BASE_URL, '')), []);
    const { ref } = useInfiniteScroll<Player>(
        10,
        componentInitialized,
        (page) => apiService.getPlayers(page, 10),
        xs => setPlayers(ps => ps.concat(xs.filter(x => !ps.some(p => p.id === x.id))))
    );

    const onPlayerDeleted = async (p: Player) => {
        const result = await apiService.deletePlayer(p.id);
        if (result.succeed) {
            setPlayers(ps => ps.filter(pl => pl.id !== p.id));
            setToastInfo({ type: 'info', messages: [`${p.name} deleted`] });
        } else {
            const messages = result.ErrorsDescriptions().length > 0 ? result.ErrorsDescriptions() : ['Server error'];
            setToastInfo({ type: 'error', messages });
        }
    };

    return (
        <>
            <div className='flex flex-col-reverse xl:flex-row items-start my-3 md:my-8'>
                <PlayersList onDelete={(p) => onPlayerDeleted(p)} players={players} ref={ref} />
                <a className='w-24' href='/players/add'>
                    <Button className='w-full'>Add</Button>
                </a>
            </div>
            {
                IsDefined(toastInfo) &&
                <Toast type={toastInfo.type} messages={toastInfo.messages} onClose={() => setToastInfo(undefined)} />
            }
        </>
    );
}