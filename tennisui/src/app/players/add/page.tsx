"use client";
import FormCard from "@/components/FormCard";
import Toast from "@/components/Toast";
import { Button } from "@/components/forms/Button";
import { Input } from "@/components/forms/Input";
import { RangeInput } from "@/components/forms/RangeInput";
import { Select } from "@/components/forms/Select";
import { IAddPlayerInput } from "@/models/IAddPlayerInput";
import { ApiService } from "@/services/ApiService/ApiService";
import { ValueOrDefault } from "@/shared/NullCheck";
import { useRouter } from "next/navigation";
import { useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";

export default function AddPlayerPage() {
    const { register, handleSubmit, formState } = useForm<IAddPlayerInput>({ reValidateMode: 'onChange' });
    const router = useRouter();
    const [toastMessages, setToastMessages] = useState<Array<string>>([]);
    const [submitted, setSubmitted] = useState(false);
    const service = new ApiService(ValueOrDefault(process.env.NEXT_PUBLIC_API_BASE_URL, ''));
    const onSubmit: SubmitHandler<IAddPlayerInput> = async (data) => {
        setSubmitted(true);
        const result = await service.createPlayer(data);
        if (result.succeed)
            router.push(`/players?added=${data.name}`);
        else {
            const messages = result.ErrorsDescriptions().length > 0 ? result.ErrorsDescriptions() : ['Server error'];
            setToastMessages(messages);
            setSubmitted(false);
        }
    };
    return (
        <>
            <div className='flex flex-row items-start justify-center mt-4 md:mt-8'>
                <FormCard className='flex flex-col items-center max-w-2xl ml-4 w-full'>
                    <h3 className='border-b border-lime-700 pb-2 text-lime-700 text-lg text-center w-full'>New Player</h3>
                    <Input
                        className='mt-2 w-full'
                        {...register('name', { required: 'Required' })}
                        label='Name'
                        error={formState.errors?.name?.message} />
                    <Select
                        className='mt-2 w-full'
                        {...register('gender', { required: 'Required' })}
                        label='Gender'
                        error={formState.errors?.name?.message}
                    >
                        <option value={'Male'}>Male</option>
                        <option value={'Female'}>Female</option>
                    </Select>
                    <RangeInput
                        className='mt-2 w-full'
                        {...register('strength')}
                        label='Strength'
                        max={100}
                        min={0}
                        step={1} />
                    <RangeInput
                        className='mt-2 w-full'
                        {...register('speed')}
                        label='Speed'
                        max={100}
                        min={0}
                        step={1} />
                    <RangeInput
                        className='mt-2 w-full'
                        {...register('reaction')}
                        label='Reaction'
                        max={100}
                        min={0}
                        step={1} />
                    <Input
                        className='mt-2 w-full'
                        {...register('image')}
                        label='Image'
                        type='file' />
                    <Button className='mt-8 w-full' loading={submitted} onClick={handleSubmit(onSubmit)}>Add</Button>
                </FormCard>
                {toastMessages.length > 0 && <Toast type='error' messages={toastMessages} onClose={() => setToastMessages([])} />}
            </div>
        </>
    );
}
