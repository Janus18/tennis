export default function Head() {
  return (
    <>
      <title>Tennis</title>
      <meta content="width=device-width, initial-scale=1" name="viewport" />
      <meta name="description" content="Tennis tournaments data" />
      <link rel="icon" href="/favicon.ico" />
    </>
  )
}
