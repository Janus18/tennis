"use client";
import { ToastType } from "@/components/Toast";
import { TournamentsInstancesList } from "@/components/tournamentsInstancesList/TournamentsInstancesList";
import useInfiniteScroll from "@/hooks/useInfiniteScroll";
import { TournamentsInstancesListItem } from "@/models/TournamentsInstancesListItem";
import { ApiService } from "@/services/ApiService/ApiService";
import { ValueOrDefault } from "@/shared/NullCheck";
import { useSearchParams } from "next/navigation";
import { useMemo, useRef, useState } from "react";

export default function Home() {
  const searchParams = useSearchParams();
  const [tournaments, setTournaments] = useState<Array<TournamentsInstancesListItem>>([]);
  const componentInitialized = useRef(false);
  const apiService = useMemo(() => new ApiService(ValueOrDefault(process.env.NEXT_PUBLIC_API_BASE_URL, '')), []);
  const { ref } = useInfiniteScroll<TournamentsInstancesListItem>(
      10,
      componentInitialized,
      (page) => apiService.getTournamentsInstances(page, 10),
      setTournaments);
  return (
    <div className='flex flex-col-reverse xl:flex-row items-start py-3 md:py-8'>
        <TournamentsInstancesList ref={ref} tournaments={tournaments} />
    </div>
  );
}
