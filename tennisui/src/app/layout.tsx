import Header from '@/components/Header';
import './globals.css';

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <head />
      <body>
        <Header />
        <div className='min-h-screen px-2.5 sm:px-8'>
          {children}
        </div>
      </body>
    </html>
  )
}
