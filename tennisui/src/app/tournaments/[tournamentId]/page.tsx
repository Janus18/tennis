"use client";
import { Button } from "@/components/forms/Button";
import { TournamentsInstancesList } from "@/components/tournamentsInstancesList/TournamentsInstancesList";
import useInfiniteScroll from "@/hooks/useInfiniteScroll";
import { TournamentsInstancesListItem } from "@/models/TournamentsInstancesListItem";
import { ApiService } from "@/services/ApiService/ApiService";
import { ValueOrDefault } from "@/shared/NullCheck";
import { useMemo, useRef, useState } from "react";

export default function TournamentInstances({ params }: { params: { tournamentId: string } }) {
    const [tournaments, setTournaments] = useState<Array<TournamentsInstancesListItem>>([]);
    const componentInitialized = useRef(false);
    const apiService = useMemo(() => new ApiService(ValueOrDefault(process.env.NEXT_PUBLIC_API_BASE_URL, '')), []);
    const { ref } = useInfiniteScroll<TournamentsInstancesListItem>(
        10,
        componentInitialized,
        (page) => apiService.getTournamentsInstances(page, 10, params.tournamentId),
        setTournaments);
    return (
        <>
            <div className='flex flex-col-reverse xl:flex-row items-start my-3 md:my-8'>
                <TournamentsInstancesList ref={ref} tournaments={tournaments} />
                <a className='w-24' href='/tournaments/add'>
                    <Button className='w-full'>Add</Button>
                </a>
            </div>
        </>
    );
}
