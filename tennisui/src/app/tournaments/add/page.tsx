"use client";
import FormCard from "@/components/FormCard";
import Toast from "@/components/Toast";
import { Button } from "@/components/forms/Button";
import { Input } from "@/components/forms/Input";
import { Select } from "@/components/forms/Select";
import { IAddTournamentInput } from "@/models/IAddTournamentInput";
import { DisplaySurfaceType, SurfaceType } from "@/models/SurfaceType";
import { ApiService } from "@/services/ApiService/ApiService";
import { GetEnumValues } from "@/shared/GetEnumValues";
import { ValueOrDefault } from "@/shared/NullCheck";
import { Range } from "@/shared/Range";
import { useRouter } from "next/navigation";
import { useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";

export default function AddTournamentPage() {
    const { register, handleSubmit, formState } = useForm<IAddTournamentInput>({ reValidateMode: 'onChange' });
    const router = useRouter();
    const [toastMessages, setToastMessages] = useState<Array<string>>([]);
    const [submitted, setSubmitted] = useState(false);
    const service = new ApiService(ValueOrDefault(process.env.NEXT_PUBLIC_API_BASE_URL, ''));
    const onSubmit: SubmitHandler<IAddTournamentInput> = async (data) => {
        setSubmitted(true);
        const result = await service.createTournament(data);
        if (result.succeed)
            router.push(`/tournaments?added=${data.name}`);
        else {
            const messages = result.ErrorsDescriptions().length > 0 ? result.ErrorsDescriptions() : ['Server error'];
            setToastMessages(messages);
            setSubmitted(false);
        }
    };
    return (
        <>
            <div className='flex flex-row items-start justify-center mt-4 md:mt-8'>
                <FormCard className='flex flex-col items-center max-w-2xl ml-4 w-full'>
                    <h3 className='border-b border-lime-700 pb-2 text-lime-700 text-lg text-center w-full'>New Tournament</h3>
                    <Input
                        className='mt-2 w-full'
                        {...register('name', { required: 'Required' })}
                        label='Name'
                        error={formState.errors?.name?.message} />
                    <Input
                        className='mt-2 w-full'
                        {...register('location', { required: 'Required' })}
                        label='Location'
                        error={formState.errors?.location?.message} />
                    <Select
                        className='mt-2 w-full'
                        {...register('surfaceType', { required: 'Required' })}
                        label='Surface'
                        error={formState.errors?.surfaceType?.message}>
                        {
                            GetEnumValues(SurfaceType).map(st =>
                                <option key={st} value={st}>{DisplaySurfaceType(SurfaceType[st])}</option>
                            )
                        }
                    </Select>
                    <Select
                        className='mt-2 w-full'
                        {...register('singlesDraw', { required: 'Required' })}
                        label='Draw'
                        error={formState.errors?.singlesDraw?.message}>
                        {
                            [2, 4, 8, 16, 32, 64, 128].map(st => <option key={st} value={st}>{st}</option>)
                        }
                    </Select>
                    <div className='flex flex-row w-full'>
                        <Select
                            className='mt-2 mr-2 w-full'
                            {...register('startDateMonth', { required: 'Required' })}
                            label='Start month'
                            error={formState.errors?.startDateMonth?.message}
                        >
                            {Range(12).map(x => x + 1).map(m => <option key={m} value={m}>{m}</option>)}
                        </Select>
                        <Select
                            className='mt-2 w-full'
                            {...register('startDateDay', { required: 'Required' })}
                            label='Start day'
                            error={formState.errors?.startDateDay?.message}
                        >
                            {Range(31).map(x => x + 1).map(d => <option key={d} value={d}>{d}</option>)}
                        </Select>
                    </div>
                    <Input
                        className='mt-2 w-full'
                        {...register('image')}
                        label='Image'
                        type='file' />
                    <Button className='mt-8 w-full' loading={submitted} onClick={handleSubmit(onSubmit)}>Add</Button>
                </FormCard>
                {toastMessages.length > 0 && <Toast type='error' messages={toastMessages} onClose={() => setToastMessages([])} />}
            </div>
        </>
    );
}
