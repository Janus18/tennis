"use client";
import Toast, { ToastType } from "@/components/Toast";
import { Button } from "@/components/forms/Button";
import { TournamentList } from "@/components/tournamentsList/TournamentList";
import useInfiniteScroll from "@/hooks/useInfiniteScroll";
import { TournamentsListItem } from "@/models/TournamentsListItem";
import { ApiService } from "@/services/ApiService/ApiService";
import { IsDefined, ValueOrDefault } from "@/shared/NullCheck";
import { useSearchParams } from "next/navigation";
import { useMemo, useRef, useState } from "react";

export default function Tournaments() {
    const searchParams = useSearchParams();
    const [toastInfo, setToastInfo] = useState<{ type: ToastType, messages: Array<string> } | undefined>(
        searchParams.has('added') ? { type: 'info', messages: [`Tournament ${searchParams.get('added')} added`] } : undefined
    );
    const [tournaments, setTournaments] = useState<Array<TournamentsListItem>>([]);
    const componentInitialized = useRef(false);
    const apiService = useMemo(() => new ApiService(ValueOrDefault(process.env.NEXT_PUBLIC_API_BASE_URL, '')), []);
    const { ref } = useInfiniteScroll<TournamentsListItem>(
        10,
        componentInitialized,
        (page) => apiService.getTournaments(page, 10),
        setTournaments);
    const onTournamentDeleted = async (t: TournamentsListItem) => {
        const result = await apiService.deleteTournament(t.id);
        if (result.succeed) {
            setTournaments(ts => ts.filter(tn => tn.id !== t.id));
            setToastInfo({ type: 'info', messages: [`Tournament ${t.name} deleted`] });
        } else {
            const messages = result.ErrorsDescriptions().length > 0 ? result.ErrorsDescriptions() : ['Server error'];
            setToastInfo({ type: 'error', messages });
        }
    };
    return (
        <>
            <div className='flex flex-col-reverse xl:flex-row items-start py-3 md:py-8'>
                <TournamentList onDelete={onTournamentDeleted} ref={ref} tournaments={tournaments} />
                <a className='w-24' href='/tournaments/add'>
                    <Button className='w-full'>Add</Button>
                </a>
            </div>
            {
                IsDefined(toastInfo) &&
                <Toast type={toastInfo.type} messages={toastInfo.messages} onClose={() => setToastInfo(undefined)} />
            }
        </>
    );
}
