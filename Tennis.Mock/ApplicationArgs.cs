using PowerArgs;
using Tennis.Mock.Commands;
using Tennis.Mock.Configuration;
using Tennis.Mock.Handlers;

namespace Tennis.Mock;

[ArgExceptionBehavior(ArgExceptionPolicy.StandardExceptionHandling)]
public class ApplicationArgs
{
    private readonly ApplicationConfiguration configuration = new();

    [ArgActionMethod, ArgDescription("Create one or more players")]
    public void CreatePlayers(CreatePlayersCommand command)
    {
        var handler = new CreatePlayersHandler(configuration);
        handler.Execute(command).Wait();
    }
}