using System.Net;
using Bogus;
using Flurl;
using Flurl.Http;
using Microsoft.Extensions.Logging;
using Tennis.Mock.Commands;
using Tennis.Mock.Configuration;

namespace Tennis.Mock.Handlers;

public class CreatePlayersHandler : IHandler<CreatePlayersCommand>
{
    private readonly Faker faker = new();
    private readonly string baseUrl;
    private readonly ILogger<CreatePlayersHandler> logger;

    public CreatePlayersHandler(ApplicationConfiguration configuration)
    {
        logger = configuration.GetLogger<CreatePlayersHandler>();
        baseUrl = configuration.Configuration["BaseUrl"]!;
    }

    public async Task Execute(CreatePlayersCommand command)
    {
        logger.LogInformation("Creating {N} players", command.Count);
        for (int i = 0; i < command.Count; i++)
        {
            var created = await ExecuteSingle((int)(command.AbilityPointsMin ?? 0), (int)(command.AbilityPointsMax ?? 100));
            if (!created)
            {
                logger.LogError("Could not create player n°{N}", i + 1);
                return;
            }
            logger.LogInformation("Player {N} created", i + 1);
        }
    }

    private async Task<bool> ExecuteSingle(int minAbilityPoints, int maxAbilityPoints)
    {
        var fileStream = await "https://loremflickr.com/320/240/tennis,player".GetStreamAsync();

        var response = await baseUrl
            .AppendPathSegments("players")
            .PostMultipartAsync(c => c
                .AddFile("Image", fileStream, "picture", "image/jpeg")
                .AddString("Name", faker.Name.FullName())
                .AddString("Strength", faker.Random.Int(minAbilityPoints, maxAbilityPoints).ToString())
                .AddString("Speed", faker.Random.Int(minAbilityPoints, maxAbilityPoints).ToString())
                .AddString("Reaction", faker.Random.Int(minAbilityPoints, maxAbilityPoints).ToString())
                .AddString("Gender", faker.PickRandom(new[] { "Male", "Female" }))
            );
        return response.StatusCode == (int)HttpStatusCode.Created;
    }
}