using Tennis.Mock.Commands;

namespace Tennis.Mock.Handlers;

public interface IHandler<TCommand> where TCommand : ICommand
{
    public abstract Task Execute(TCommand command);
}