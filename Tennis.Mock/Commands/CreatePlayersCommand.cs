using PowerArgs;

namespace Tennis.Mock.Commands;

public class CreatePlayersCommand : ICommand
{
    [ArgShortcut("n"), ArgDescription("How many players to create")]
    public uint Count { get; set; } = 1;
    
    [ArgShortcut("min"), ArgDescription("Minimum possible value for the abilities")]
    public uint? AbilityPointsMin { get; set; }
    
    [ArgShortcut("max"), ArgDescription("Maximum possible value for the abilities")]
    public uint? AbilityPointsMax { get; set; }
}