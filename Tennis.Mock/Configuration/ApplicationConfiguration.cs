using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;

namespace Tennis.Mock.Configuration;

public class ApplicationConfiguration
{
    private readonly ILoggerFactory loggerFactory;

    public IConfiguration Configuration { get; }

    public ApplicationConfiguration()
    {
        Configuration = new ConfigurationBuilder()
            .AddUserSecrets<ApplicationConfiguration>()
            .Build();
        var serilog = new LoggerConfiguration()
            .WriteTo.Console(Serilog.Events.LogEventLevel.Information)
            .CreateLogger();
        loggerFactory = new LoggerFactory().AddSerilog(serilog);
    }

    public ILogger<T> GetLogger<T>() => loggerFactory.CreateLogger<T>();
}