using Tennis.Domain.Contract.DataAccess;
using Tennis.Domain.Player;

namespace Tennis.Infrastructure.DataAccess.Implementation;

public class PlayerRepository : BaseRepository<Player>, IPlayerRepository
{
    public PlayerRepository(Context context) : base(context)
    {
    }
}
