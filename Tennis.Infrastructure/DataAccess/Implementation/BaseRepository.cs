using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Tennis.Domain.Contract;
using Tennis.Domain.Contract.DataAccess;
using Tennis.Domain.ValueObjects;

namespace Tennis.Infrastructure.DataAccess.Implementation;

public abstract class BaseRepository<TAggregate> : IRepository<TAggregate> where TAggregate : class, IAggregate
{
    protected readonly Context Context;

    protected BaseRepository(Context context)
    {
        this.Context = context;
    }

    public virtual void Add(TAggregate aggregate)
    {
        Context.Add(aggregate);
    }

    public async Task AddAsync(TAggregate aggregate)
    {
        Context.Add(aggregate);
        await Context.SaveChangesAsync();
    }

    public void Delete(TAggregate aggregate)
    {
        Context.Remove(aggregate);
    }

    public async Task DeleteAsync(TAggregate aggregate)
    {
        Context.Remove(aggregate);
        await Context.SaveChangesAsync();
    }

    public Task<TAggregate?> Get(
        Expression<Func<TAggregate, bool>> predicate,
        IEnumerable<Expression<Func<TAggregate, object>>>? include = null
    )
    {
        IQueryable<TAggregate> query = Query(predicate, include);
        return query.FirstOrDefaultAsync();
    }

    public async Task<IEnumerable<TAggregate>> GetMany(
        Expression<Func<TAggregate, bool>> predicate,
        IEnumerable<Expression<Func<TAggregate, object>>>? include = null,
        IOrder<TAggregate>? order = null,
        Page? page = null
    )
    {
        var query = Query(predicate, include);
        if (order != null)
        {
            query = order.Order(query);
        }
        if (page.HasValue)
        {
            query = query.Skip(page.Value.Count * page.Value.Number).Take(page.Value.Count);
        }
        return await query.ToListAsync();
    }

    public Task<bool> Exists(Expression<Func<TAggregate, bool>> predicate)
        => Query(predicate).AnyAsync();

    public async Task<uint> Count(Expression<Func<TAggregate, bool>> predicate)
        => (uint) await Query(predicate).CountAsync();

    public Task SaveChanges()
        => Context.SaveChangesAsync();

    public void Update(TAggregate aggregate)
        => Context.Update(aggregate);

    public async Task UpdateAsync(TAggregate aggregate)
    {
        Update(aggregate);
        await SaveChanges();
    }

    protected IQueryable<T> Query<T>(
        Expression<Func<T, bool>> predicate,
        IEnumerable<Expression<Func<T, object>>>? include = null
    ) where T : class
    {
        var query = Context.Set<T>().Where(predicate);
        return include == null
            ? query
            : include.Aggregate(query, (current, x) => current.Include(x));
    }
}
