using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Tennis.Domain.Contract.DataAccess;
using Tennis.Domain.Player;
using Tennis.Domain.Tournament;
using Tennis.Domain.ValueObjects;
using Tennis.Infrastructure.DataAccess.Contract;
using Tennis.Infrastructure.Dto;

namespace Tennis.Infrastructure.DataAccess.Implementation;

public class TournamentRepository : BaseRepository<Tournament>, ITournamentRepository
{
    public TournamentRepository(Context context) : base(context)
    {
    }
}
