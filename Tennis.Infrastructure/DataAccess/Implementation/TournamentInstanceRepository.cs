using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Tennis.Domain.Contract.DataAccess;
using Tennis.Domain.Match;
using Tennis.Domain.Player;
using Tennis.Domain.Tournament;
using Tennis.Domain.ValueObjects;
using Tennis.Infrastructure.DataAccess.Contract;
using Tennis.Infrastructure.Dto;

namespace Tennis.Infrastructure.DataAccess.Implementation;

public class TournamentInstanceRepository : BaseRepository<TournamentInstance>, IExtendedTournamentInstanceRepository
{
    public TournamentInstanceRepository(Context context) : base(context)
    {
    }

    public Task<TournamentInstance?> Get(Guid tournamentInstanceId)
        => Context.Set<TournamentInstance>()
            .Include(t => t.Tournament)
            .Include(t => t.Matches).ThenInclude(m => m.Player1)
            .Include(t => t.Matches).ThenInclude(m => m.Player2)
            .Include(t => t.Matches).ThenInclude(m => m.Winner)
            .FirstOrDefaultAsync(t => t.Id == tournamentInstanceId);

    public Task<bool> Exists(Guid tournamentId, int year)
        => Context.Set<TournamentInstance>()
            .Where(t => t.Tournament.Id == tournamentId)
            .Where(t => t.StartDate.Year == year)
            .AnyAsync();
    
    public Task<int> OngoingTournaments()
        => Context.Set<TournamentInstance>().CountAsync(t => !t.IsFinished);
    
    public async Task<IList<PastTournamentInstanceDto>> ListPastTournaments(
        Expression<Func<TournamentInstance, bool>> predicate,
        Page page,
        IOrder<TournamentInstance>? order = null
    )
    {
        var query = Query(predicate).Where(t => t.Winner != null);
        if (order != null)
            query = order.Order(query);
        
        return await query
            .Skip(page.Count * page.Number).Take(page.Count)
            .Select(t =>
                new PastTournamentInstanceDto
                {
                    Id = t.Id,
                    TournamentId = t.Tournament.Id,
                    StartDate = t.StartDate,
                    EndDate = t.EndDate,
                    Gender = t.DisplayGender,
                    ImageUri = t.Tournament.DisplayImageUri,
                    Location = t.Tournament.Location,
                    Name = t.Tournament.Name.Value,
                    SinglesDraw = t.Tournament.SinglesDraw,
                    SurfaceType = t.Tournament.SurfaceType.ToString(),
                    Winner = new TournamentPlayerDto(t.Winner!.Id, t.Winner.Name.Value, t.Winner.ImageUri)
                })
            .ToListAsync();
    }
    
    public async Task<IList<CurrentTournamentInstanceDto>> ListCurrentTournaments(
        Expression<Func<TournamentInstance, bool>> predicate,
        Page page,
        IOrder<TournamentInstance>? order = null
    )
    {
        var query = Query(predicate).Where(t => t.Winner == null);
        if (order != null)
            query = order.Order(query);

        var tournaments = await query
            .Skip(page.Count * page.Number).Take(page.Count)
            .Include(t => t.Matches)
                .ThenInclude(m => m.Player1)
            .Include(t => t.Matches)
                .ThenInclude(m => m.Player2)
            .ToListAsync();

        return tournaments.Select(t => new CurrentTournamentInstanceDto(t)).ToList();
    }
}
