﻿using System.Security.Cryptography.X509Certificates;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using NodaTime;
using Tennis.Domain.Match;
using Tennis.Domain.Player;
using Tennis.Domain.Tournament;

namespace Tennis.Infrastructure.DataAccess;

public class Context : DbContext
{
    public Context(DbContextOptions<Context> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        AddPlayer(modelBuilder);
        AddMatch(modelBuilder);
        AddTournament(modelBuilder);
        AddTournamentInstance(modelBuilder);
    }

    private static void AddPlayer(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Player>(player =>
        {
            player.HasKey(p => p.Id);
            player.Property(p => p.Name)
                .IsRequired()
                .HasConversion(n => n.Value, n => new PlayerName(n));
            player.Property(p => p.Gender).HasConversion(new EnumToStringConverter<Gender>());
            player.Property(p => p.Strength)
                .HasConversion(
                    s => s.Value,
                    v => AbilityPoints.Create(v));
            player.Property(p => p.Reaction)
                .HasConversion(
                    s => s.Value,
                    v => AbilityPoints.Create(v));
            player.Property(p => p.Speed)
                .HasConversion(
                    s => s.Value,
                    v => AbilityPoints.Create(v));
        });
    }

    private static void AddMatch(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Match>(match =>
        {
            match.HasKey(m => m.Id);
            match.HasOne(m => m.Player1).WithMany().OnDelete(DeleteBehavior.Restrict);
            match.HasOne(m => m.Player2).WithMany().OnDelete(DeleteBehavior.Restrict);
            match.Property(m => m.Round);
            match.Property(m => m.Date);
            match.HasOne(m => m.Winner).WithMany().OnDelete(DeleteBehavior.Restrict);
            match.HasIndex("WinnerId").HasFilter("WinnerId IS NOT NULL");
        });
    }

    private static void AddTournamentInstance(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<TournamentInstance>(tournamentInst =>
        {
            tournamentInst.HasKey(t => t.Id);
            tournamentInst.HasMany(t => t.Matches)
                .WithOne()
                .OnDelete(DeleteBehavior.Cascade);
            tournamentInst.Property(t => t.Gender).HasConversion(new EnumToStringConverter<Gender>());
            tournamentInst.HasOne(t => t.Tournament).WithMany().OnDelete(DeleteBehavior.Cascade);
            tournamentInst.Metadata.FindNavigation(nameof(TournamentInstance.Matches))?
                .SetPropertyAccessMode(PropertyAccessMode.Field);
            tournamentInst.HasIndex(t => t.StartDate);
        });
    }

    private static void AddTournament(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Tournament>(tournament =>
        {
            tournament.HasKey(t => t.Id);
            tournament.Property(t => t.Name)
                .HasMaxLength(100)
                .IsRequired()
                .HasConversion(n => n.Value, n => new TournamentName(n));
            tournament.Property(t => t.Location).HasMaxLength(200).IsRequired();
            tournament.Property(t => t.SurfaceType).HasConversion(new EnumToStringConverter<Surfaces>());
            tournament.Property(t => t.StartDateDay).IsRequired();
            tournament.Property(t => t.StartDateMonth).IsRequired();
            tournament.Ignore(t => t.StartDate);
        });
    }
}
