﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Tennis.Infrastructure.DataAccess.Migrations
{
    public partial class AddTournamentImageUri : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ImageUri",
                table: "Tournament",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ImageUri",
                table: "Tournament");
        }
    }
}
