﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Tennis.Infrastructure.DataAccess.Migrations
{
    public partial class TournamentToInstance : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Match_Tournament_TournamentId",
                table: "Match");

            migrationBuilder.DropTable(
                name: "Tournament");

            migrationBuilder.RenameColumn(
                name: "TournamentId",
                table: "Match",
                newName: "TournamentInstanceId");

            migrationBuilder.RenameIndex(
                name: "IX_Match_TournamentId",
                table: "Match",
                newName: "IX_Match_TournamentInstanceId");

            migrationBuilder.CreateTable(
                name: "TournamentInstance",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Gender = table.Column<int>(type: "int", nullable: true),
                    Date = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TournamentInstance", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Match_TournamentInstance_TournamentInstanceId",
                table: "Match",
                column: "TournamentInstanceId",
                principalTable: "TournamentInstance",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Match_TournamentInstance_TournamentInstanceId",
                table: "Match");

            migrationBuilder.DropTable(
                name: "TournamentInstance");

            migrationBuilder.RenameColumn(
                name: "TournamentInstanceId",
                table: "Match",
                newName: "TournamentId");

            migrationBuilder.RenameIndex(
                name: "IX_Match_TournamentInstanceId",
                table: "Match",
                newName: "IX_Match_TournamentId");

            migrationBuilder.CreateTable(
                name: "Tournament",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Gender = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tournament", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Match_Tournament_TournamentId",
                table: "Match",
                column: "TournamentId",
                principalTable: "Tournament",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
