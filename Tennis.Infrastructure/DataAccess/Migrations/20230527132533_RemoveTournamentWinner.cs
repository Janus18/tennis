﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Tennis.Infrastructure.DataAccess.Migrations
{
    public partial class RemoveTournamentWinner : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tournament_Player_WinnerId",
                table: "Tournament");

            migrationBuilder.DropIndex(
                name: "IX_Tournament_WinnerId",
                table: "Tournament");

            migrationBuilder.DropColumn(
                name: "WinnerId",
                table: "Tournament");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "WinnerId",
                table: "Tournament",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Tournament_WinnerId",
                table: "Tournament",
                column: "WinnerId",
                filter: "WinnerId IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Tournament_Player_WinnerId",
                table: "Tournament",
                column: "WinnerId",
                principalTable: "Player",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
