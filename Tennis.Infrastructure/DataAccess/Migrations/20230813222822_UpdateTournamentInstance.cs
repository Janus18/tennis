﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Tennis.Infrastructure.DataAccess.Migrations
{
    public partial class UpdateTournamentInstance : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "EndDate",
                table: "TournamentInstance",
                type: "date",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<Guid>(
                name: "WinnerId",
                table: "TournamentInstance",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TournamentInstance_StartDate",
                table: "TournamentInstance",
                column: "StartDate");

            migrationBuilder.CreateIndex(
                name: "IX_TournamentInstance_WinnerId",
                table: "TournamentInstance",
                column: "WinnerId");

            migrationBuilder.AddForeignKey(
                name: "FK_TournamentInstance_Player_WinnerId",
                table: "TournamentInstance",
                column: "WinnerId",
                principalTable: "Player",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TournamentInstance_Player_WinnerId",
                table: "TournamentInstance");

            migrationBuilder.DropIndex(
                name: "IX_TournamentInstance_StartDate",
                table: "TournamentInstance");

            migrationBuilder.DropIndex(
                name: "IX_TournamentInstance_WinnerId",
                table: "TournamentInstance");

            migrationBuilder.DropColumn(
                name: "EndDate",
                table: "TournamentInstance");

            migrationBuilder.DropColumn(
                name: "WinnerId",
                table: "TournamentInstance");
        }
    }
}
