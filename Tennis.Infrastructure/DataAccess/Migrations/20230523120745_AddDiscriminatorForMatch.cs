﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Tennis.Infrastructure.DataAccess.Migrations
{
    public partial class AddDiscriminatorForMatch : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "MatchNumber",
                table: "Match",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Type",
                table: "Match",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MatchNumber",
                table: "Match");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "Match");
        }
    }
}
