using System.Linq.Expressions;
using Tennis.Domain.Contract.DataAccess;

namespace Tennis.Infrastructure.DataAccess.Order;

public static class Order<T>
{
    public static IOrder<T> By<T2>(Expression<Func<T, T2>> exp)
    {
        return new OrderBy<T, T2>(exp);
    }

    public static IOrder<T> ByDesc<T2>(Expression<Func<T, T2>> exp)
    {
        return new OrderByDescending<T, T2>(exp);
    }
}
