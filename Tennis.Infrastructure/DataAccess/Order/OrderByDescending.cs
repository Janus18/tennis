using System.Linq.Expressions;
using Tennis.Domain.Contract.DataAccess;

namespace Tennis.Infrastructure.DataAccess.Order;

public class OrderByDescending<T, T2> : BaseOrder<T, T2>
{
    public OrderByDescending(Expression<Func<T, T2>> exp, IOrder<T>? parent = null) : base(exp, parent)
    {
    }

    public override IOrderedQueryable<T> By(IQueryable<T> query) => query.OrderByDescending(exp);

    public override IOrderedQueryable<T> ThenBy(IOrderedQueryable<T> query) => query.ThenByDescending(exp);
}
