using System.Linq.Expressions;
using Tennis.Domain.Contract.DataAccess;

namespace Tennis.Infrastructure.DataAccess.Order;

public abstract class BaseOrder<T, T2> : IOrder<T>, IOrderBy<T>
{
    protected readonly Expression<Func<T, T2>> exp;

    protected readonly IOrder<T>? parent;

    public BaseOrder(Expression<Func<T, T2>> exp, IOrder<T>? parent = null)
    {
        this.exp = exp;
        this.parent = parent;
    }

    public IOrder<T> ThenBy<T3>(Expression<Func<T, T3>> exp)
        => new OrderBy<T, T3>(exp, this);

    public IOrder<T> ThenByDesc<T3>(Expression<Func<T, T3>> exp)
        => new OrderByDescending<T, T3>(exp, this);

    public IOrderedQueryable<T> Order(IQueryable<T> query)
    {
        var orders = Collect(new List<IOrderBy<T>>());
        var orderedQuery = orders.First().By(query);
        foreach (var order in orders.Skip(1))
        {
            orderedQuery = order.ThenBy(orderedQuery);
        }
        return orderedQuery;
    }

    public abstract IOrderedQueryable<T> By(IQueryable<T> query);

    public abstract IOrderedQueryable<T> ThenBy(IOrderedQueryable<T> query);

    public IList<IOrderBy<T>> Collect(IList<IOrderBy<T>> orders)
    {
        var newOrders = orders.Prepend(this).ToList();
        return parent is null ? newOrders : parent.Collect(newOrders);
    }
}
