using System.Linq.Expressions;
using Tennis.Domain.Contract.DataAccess;
using Tennis.Domain.Tournament;
using Tennis.Domain.ValueObjects;
using Tennis.Infrastructure.Dto;

namespace Tennis.Infrastructure.DataAccess.Contract;

public interface IExtendedTournamentInstanceRepository : ITournamentInstanceRepository
{
    Task<IList<PastTournamentInstanceDto>> ListPastTournaments(
        Expression<Func<TournamentInstance, bool>> predicate,
        Page page,
        IOrder<TournamentInstance>? order = null
    );
    
    Task<IList<CurrentTournamentInstanceDto>> ListCurrentTournaments(
        Expression<Func<TournamentInstance, bool>> predicate,
        Page page,
        IOrder<TournamentInstance>? order = null
    );
}
