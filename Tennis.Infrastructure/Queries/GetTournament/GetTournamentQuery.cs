using MediatR;

namespace Tennis.Infrastructure.Queries.GetTournament;

public class GetTournamentQuery : IRequest<GetTournamentOutput?>
{
    public Guid Id { get; }

    public GetTournamentQuery(Guid id)
    {
        Id = id;
    }
}