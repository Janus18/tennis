using MediatR;
using Tennis.Domain.Contract.DataAccess;

namespace Tennis.Infrastructure.Queries.GetTournament;

public class GetTournamentHandler : IRequestHandler<GetTournamentQuery, GetTournamentOutput?>
{
    private readonly ITournamentRepository _tournamentRepository;

    public GetTournamentHandler(ITournamentRepository tournamentRepository)
    {
        _tournamentRepository = tournamentRepository;
    }
    
    public async Task<GetTournamentOutput?> Handle(GetTournamentQuery request, CancellationToken cancellationToken)
    {
        var tournament = await _tournamentRepository.Get(t => t.Id == request.Id);
        if (tournament is null)
            return null;
        return new GetTournamentOutput
        {
            Id = tournament.Id,
            ImageUri = tournament.DisplayImageUri,
            Location = tournament.Location,
            Name = tournament.Name.Value,
            SinglesDraw = tournament.SinglesDraw,
            StartDate = tournament.StartDate,
            SurfaceType = tournament.SurfaceType.ToString(),
        };
    }
}