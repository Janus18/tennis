using MediatR;
using Tennis.Domain.Player;
using Tennis.Infrastructure.Dto;

namespace Tennis.Infrastructure.Queries.ListPlayers;

public class ListPlayersQuery : IRequest<IList<PlayerOutputDto>>
{
    public ushort Page { get; }
    public ushort Count { get; }
    public Gender? Gender { get; }
    public string? Name { get; }

    public ListPlayersQuery(ushort page, ushort count, Gender? gender, string? name)
    {
        Page = page;
        Count = count;
        Gender = gender;
        Name = name;
    }
}