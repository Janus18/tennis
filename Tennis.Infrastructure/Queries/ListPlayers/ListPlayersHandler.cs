using System.Linq.Expressions;
using MediatR;
using Tennis.Domain.Contract.DataAccess;
using Tennis.Domain.Player;
using Tennis.Domain.ValueObjects;
using Tennis.Infrastructure.DataAccess.Extensions;
using Tennis.Infrastructure.DataAccess.Order;
using Tennis.Infrastructure.Dto;

namespace Tennis.Infrastructure.Queries.ListPlayers;

public class ListPlayersHandler : IRequestHandler<ListPlayersQuery, IList<PlayerOutputDto>>
{
    private readonly IPlayerRepository _playerRepository;

    public ListPlayersHandler(IPlayerRepository playerRepository)
    {
        _playerRepository = playerRepository;
    }
    
    public async Task<IList<PlayerOutputDto>> Handle(ListPlayersQuery request, CancellationToken cancellationToken)
    {
        var order = new OrderBy<Player, PlayerName>(p => p.Name);
        var page = new Page(request.Page, request.Count);
        var result = await _playerRepository.GetMany(GetPredicate(request), order: order, page: page);
        return result.Select(p => new PlayerOutputDto
        {
            Gender = p.Gender.ToString(),
            Id = p.Id,
            Name = p.Name.Value,
            Reaction = p.Reaction.Value,
            Speed = p.Speed.Value,
            Strength = p.Strength.Value,
            ImageUri = p.ImageUriAbsolute
        }).ToList();
    }

    private static Expression<Func<Player, bool>> GetPredicate(ListPlayersQuery query)
    {
        Expression<Func<Player, bool>> predicate = p => true;
        if (!string.IsNullOrWhiteSpace(query.Name))
            predicate = predicate.And(p => p.Name.Value.Contains(query.Name));
        if (query.Gender.HasValue)
            predicate = predicate.And(p => p.Gender == query.Gender.Value);
        return predicate;
    }
}