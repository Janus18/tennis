using MediatR;
using Tennis.Domain.Contract.DataAccess;
using Tennis.Domain.Tournament;
using Tennis.Domain.ValueObjects;
using Tennis.Infrastructure.DataAccess.Order;

namespace Tennis.Infrastructure.Queries.ListTournaments;

public class ListTournamentsHandler : IRequestHandler<ListTournamentsQuery, IEnumerable<ListTournamentsResponseItem>>
{
    private readonly ITournamentRepository _tournamentRepository;

    public ListTournamentsHandler(ITournamentRepository tournamentRepository)
    {
        _tournamentRepository = tournamentRepository;
    }
    
    public async Task<IEnumerable<ListTournamentsResponseItem>> Handle(ListTournamentsQuery request, CancellationToken cancellationToken)
    {
        var tournaments = await _tournamentRepository.GetMany(
            t => true,
            order: new OrderBy<Tournament, TournamentName>(t => t.Name),
            page: new Page(request.Page, request.Count)
        );
        return tournaments.Select(t => new ListTournamentsResponseItem
        {
            Id = t.Id,
            ImageUri = t.DisplayImageUri,
            Location = t.Location,
            Name = t.Name.Value,
            SinglesDraw = t.SinglesDraw,
            StartDate = t.StartDate,
            SurfaceType = t.SurfaceType.ToString(),
        }).ToList();
    }
}