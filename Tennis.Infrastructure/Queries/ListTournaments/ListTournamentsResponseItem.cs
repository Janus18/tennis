using NodaTime;

namespace Tennis.Infrastructure.Queries.ListTournaments;

public class ListTournamentsResponseItem
{
    public Guid Id { get; init; }
    public string Name { get; init; } = "";
    public string SurfaceType { get; init; } = "";
    public string Location { get; init; } = "";
    public uint SinglesDraw { get; init; }
    public AnnualDate StartDate { get; init; }
    public string? ImageUri { get; init; }
}