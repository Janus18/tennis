using MediatR;

namespace Tennis.Infrastructure.Queries.ListTournaments;

public class ListTournamentsQuery : IRequest<IEnumerable<ListTournamentsResponseItem>>
{
    public ushort Count { get; }
    public ushort Page { get; }
    
    public ListTournamentsQuery(ushort count, ushort page)
    {
        Count = count;
        Page = page;
    }
}