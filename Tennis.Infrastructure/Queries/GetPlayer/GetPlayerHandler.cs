using MediatR;
using Tennis.Domain.Contract.DataAccess;
using Tennis.Infrastructure.Dto;

namespace Tennis.Infrastructure.Queries.GetPlayer;

public class GetPlayerHandler : IRequestHandler<GetPlayerQuery, PlayerOutputDto?>
{
    private readonly IPlayerRepository _playerRepository;

    public GetPlayerHandler(IPlayerRepository playerRepository)
    {
        _playerRepository = playerRepository;
    }
    
    public async Task<PlayerOutputDto?> Handle(GetPlayerQuery request, CancellationToken cancellationToken)
    {
        var player = await _playerRepository.Get(p => p.Id == request.PlayerId);
        if (player is null)
            return null;
        return new PlayerOutputDto
        {
            Id = player.Id,
            Gender = player.Gender.ToString(),
            Name = player.Name.Value,
            Reaction = player.Reaction.Value,
            Speed = player.Speed.Value,
            Strength = player.Strength.Value,
            ImageUri = player.ImageUriAbsolute
        };
    }
}