using MediatR;
using Tennis.Infrastructure.Dto;

namespace Tennis.Infrastructure.Queries.GetPlayer;

public class GetPlayerQuery : IRequest<PlayerOutputDto?>
{
    public Guid PlayerId { get; }

    public GetPlayerQuery(Guid playerId)
    {
        PlayerId = playerId;
    }
}