using MediatR;
using Tennis.Infrastructure.Dto;

namespace Tennis.Infrastructure.Queries.GetTournamentInstance;

public class GetTournamentInstanceQuery : IRequest<TournamentInstanceOutput?>
{
    public Guid Id { get; }

    public GetTournamentInstanceQuery(Guid id)
    {
        Id = id;
    }
}