using System.Collections.Immutable;
using Tennis.Domain.Player;
using Tennis.Domain.Contract.DataAccess;
using MediatR;

namespace Tennis.Infrastructure.Queries.GetTournamentInstance;

public class GetTournamentInstanceHandler : IRequestHandler<GetTournamentInstanceQuery, TournamentInstanceOutput?>
{
    private readonly ITournamentInstanceRepository _tournamentInstanceRepository;

    public GetTournamentInstanceHandler(ITournamentInstanceRepository tournamentInstanceRepository)
    {
        _tournamentInstanceRepository = tournamentInstanceRepository;
    }

    public async Task<TournamentInstanceOutput?> Handle(GetTournamentInstanceQuery request, CancellationToken cancellationToken)
    {
        var tournament = await _tournamentInstanceRepository.Get(request.Id);
        if (tournament is null)
            return null;
        return new TournamentInstanceOutput
        {
            Id = tournament.Id,
            Gender = tournament.DisplayGender,
            Date = tournament.StartDate,
            Winner = MapPlayer(tournament.Matches.OrderByDescending(m => m.Round).First().Winner),
            Matches = tournament.Matches.Select(m => new TournamentMatchOutput
            {
                Date = m.Date,
                Id = m.Id,
                Round = m.Round,
                Winner = MapPlayer(m.Winner),
                Player1 = MapPlayer(m.Player1),
                Player2 = MapPlayer(m.Player2),
            }).ToImmutableList()
        };
    }

    private static TournamentPlayerOutput? MapPlayer(Player? player)
        => player is not null ? new TournamentPlayerOutput { Id = player.Id, Name = player.Name.Value } : null;
}