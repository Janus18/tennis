using System.Collections.Immutable;
using NodaTime;

namespace Tennis.Infrastructure.Queries.GetTournamentInstance;

public class TournamentInstanceOutput
{
    public Guid Id { get; init; }

    public string Gender { get; init; } = "";

    public LocalDate Date { get; init; }

    public IImmutableList<TournamentMatchOutput> Matches { get; init; } = ImmutableList<TournamentMatchOutput>.Empty;

    public TournamentPlayerOutput? Winner { get; init; }
}

public class TournamentMatchOutput
{
    public Guid Id { get; init; }

    public LocalDate Date { get; init; }

    public TournamentPlayerOutput? Player1 { get; init; }

    public TournamentPlayerOutput? Player2 { get; init; }

    public int Round { get; init; }

    public TournamentPlayerOutput? Winner { get; init; }
}

public class TournamentPlayerOutput
{
    public Guid Id { get; init; }

    public string Name { get; init; } = "";
}
