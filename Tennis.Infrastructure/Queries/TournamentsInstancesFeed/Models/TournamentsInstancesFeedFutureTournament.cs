using NodaTime;
using Tennis.Domain.Tournament;

namespace Tennis.Infrastructure.Queries.TournamentsInstancesFeed.Models;

public record TournamentsInstancesFeedFutureTournament
{
    public Guid Id { get; }

    public LocalDate StartDate { get; }
    
    public string Name { get; }

    public string SurfaceType { get; }
    
    public string Location { get; }
    
    public uint SinglesDraw { get; }
    
    public string? ImageUri { get; }

    public TournamentsInstancesFeedFutureTournament(Tournament tournament)
    {
        Id = tournament.Id;
        StartDate = tournament.NextPlay;
        Name = tournament.Name.Value;
        SurfaceType = tournament.SurfaceType.ToString();
        Location = tournament.Location;
        SinglesDraw = tournament.SinglesDraw;
        ImageUri = tournament.DisplayImageUri;
    }
}