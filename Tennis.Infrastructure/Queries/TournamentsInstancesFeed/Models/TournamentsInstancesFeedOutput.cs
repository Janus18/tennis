using Tennis.Infrastructure.Dto;

namespace Tennis.Infrastructure.Queries.TournamentsInstancesFeed.Models;

public record TournamentsInstancesFeedOutput
{
    public IList<CurrentTournamentInstanceDto> CurrentTournaments { get; init; }
    
    public IList<PastTournamentInstanceDto> PastTournaments { get; init; }
    
    public IList<TournamentsInstancesFeedFutureTournament> FutureTournaments { get; init; }
}