using MediatR;
using Tennis.Infrastructure.Queries.TournamentsInstancesFeed.Models;

namespace Tennis.Infrastructure.Queries.TournamentsInstancesFeed;

public class TournamentsInstancesFeedQuery : IRequest<TournamentsInstancesFeedOutput>
{
    public ushort CurrentCount { get; }
    public ushort PastCount { get; }
    public ushort FutureCount { get; }

    public TournamentsInstancesFeedQuery(ushort currentCount, ushort pastCount, ushort futureCount)
    {
        CurrentCount = currentCount;
        PastCount = pastCount;
        FutureCount = futureCount;
    }
}