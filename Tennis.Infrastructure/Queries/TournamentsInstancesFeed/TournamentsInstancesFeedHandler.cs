using MediatR;
using NodaTime;
using Tennis.Domain.Contract.DataAccess;
using Tennis.Domain.Tournament;
using Tennis.Domain.ValueObjects;
using Tennis.Infrastructure.DataAccess.Contract;
using Tennis.Infrastructure.DataAccess.Order;
using Tennis.Infrastructure.Queries.TournamentsInstancesFeed.Models;

namespace Tennis.Infrastructure.Queries.TournamentsInstancesFeed;

public class TournamentsInstancesFeedHandler : IRequestHandler<TournamentsInstancesFeedQuery, TournamentsInstancesFeedOutput>
{
    private readonly ITournamentRepository _tournamentRepository;
    private readonly IExtendedTournamentInstanceRepository _tournamentInstanceRepository;

    public TournamentsInstancesFeedHandler(
        ITournamentRepository tournamentRepository,
        IExtendedTournamentInstanceRepository tournamentInstanceInstanceRepository
    )
    {
        _tournamentRepository = tournamentRepository;
        _tournamentInstanceRepository = tournamentInstanceInstanceRepository;
    }
    
    public async Task<TournamentsInstancesFeedOutput> Handle(TournamentsInstancesFeedQuery request, CancellationToken cancellationToken)
    {
        var pastTournaments = await _tournamentInstanceRepository.ListPastTournaments(
            t => true,
            new Page(0, request.PastCount),
            new OrderByDescending<TournamentInstance, LocalDate>(t => t.EndDate)
        );
        var currentTournaments = await _tournamentInstanceRepository.ListCurrentTournaments(
            t => true,
            new Page(0, request.CurrentCount),
            new OrderByDescending<TournamentInstance, LocalDate>(t => t.StartDate)
        );
        var currentTournamentsIds = currentTournaments.Select(t => t.TournamentId).ToHashSet();
        var tournaments = await _tournamentRepository.GetMany(
            t => !currentTournamentsIds.Contains(t.Id),
            order: new OrderBy<Tournament, int>(t => t.StartDateMonth).ThenBy(t => t.StartDateDay)
        );
        return new TournamentsInstancesFeedOutput
        {
            CurrentTournaments = currentTournaments,
            FutureTournaments = tournaments.Select(t => new TournamentsInstancesFeedFutureTournament(t)).ToList(),
            PastTournaments = pastTournaments
        };
    }
}