using Amazon.S3;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Tennis.Domain.Contract.DataAccess;
using Tennis.Domain.Contract.Services;
using Tennis.Infrastructure.DataAccess;
using Tennis.Infrastructure.DataAccess.Contract;
using Tennis.Infrastructure.DataAccess.Implementation;
using Tennis.Infrastructure.Services.Contract;
using Tennis.Infrastructure.Services.Implementation;

namespace Tennis.Infrastructure;

public static class InfrastructureDeps
{
    public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddAWSService<IAmazonS3>();
        services.AddScoped<IPlayerRepository, PlayerRepository>();
        services.AddScoped<ITournamentRepository, TournamentRepository>();
        services.AddScoped<ITournamentInstanceRepository, TournamentInstanceRepository>();
        services.AddScoped<IExtendedTournamentInstanceRepository, TournamentInstanceRepository>();
        services.AddSingleton<IRandomService, RandomService>();
        services.AddSingleton<IStorageService, StorageService>();
        services.AddSingleton<ITournamentStorageService, TournamentStorageService>();
        services.AddSingleton<IPlayerStorageService, PlayerStorageService>();
        services.AddAutoMapper(typeof(InfrastructureDeps));
        services.AddMediatR(cf => cf.RegisterServicesFromAssembly(typeof(InfrastructureDeps).Assembly));
        var connectionString = configuration.GetConnectionString("Database");
        services.AddSqlServer<Context>(connectionString, x => x.UseNodaTime());
        return services;
    }
}