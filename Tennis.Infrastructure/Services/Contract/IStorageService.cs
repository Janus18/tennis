using File = Tennis.Infrastructure.Dto.File;

namespace Tennis.Infrastructure.Services.Contract;

public interface IStorageService
{
    Task<Uri> Save(File file);
    Task Delete(string name);
}