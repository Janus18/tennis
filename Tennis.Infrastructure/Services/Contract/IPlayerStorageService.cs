using Tennis.Domain.Player;

namespace Tennis.Infrastructure.Services.Contract;

public interface IPlayerStorageService
{
    Task<Uri> Save(Player player, Dto.File file);
    Task Delete(Player player);
}
