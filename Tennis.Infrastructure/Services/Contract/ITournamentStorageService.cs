using Tennis.Domain.Tournament;
using File = Tennis.Infrastructure.Dto.File;

namespace Tennis.Infrastructure.Services.Contract;

public interface ITournamentStorageService
{
    Task<Uri> Save(Tournament tournament, File file);
    Task Delete(Tournament tournament);
}