using Tennis.Domain.Contract.Services;
using Tennis.Domain.ValueObjects;

namespace Tennis.Infrastructure.Services.Implementation;

public class RandomService : IRandomService
{
    private static readonly Random Random = new();
    private static readonly object RandomLock = new { };

    /// <summary>
    /// The idea of the implementation is to give each value a number of "tickets" equal to
    /// its weight. Then we choose one of those tickets randomly and we find the "winner".
    /// </summary>
    /// <param name="values"></param>
    /// <typeparam name="T"></typeparam>
    public T Choose<T>(IList<WeightedValue<T>> values)
    {
        var totalWeight = values.Select(v => v.Weight).Sum();
        var winningTicket = RandomInt(0, totalWeight - 1);
        var previousSum = (Index: 0, Sum: 0);
        do
        {
            var currentSum = values.ElementAt(previousSum.Index).Weight + previousSum.Sum;
            if (winningTicket < currentSum)
                return values.ElementAt(previousSum.Index).Value;
            previousSum = (previousSum.Index + 1, currentSum);
        } while (previousSum.Index < values.Count);

        return values.Last().Value;
    }

    /// <summary>
    /// Random is not thread safe 
    /// </summary>
    /// <param name="min"></param>
    /// <param name="max"></param>
    /// <returns>Random int</returns>
    private static int RandomInt(int min, int max)
    {
        lock (RandomLock)
        {
            return Random.Next(min, max);
        }
    }
}