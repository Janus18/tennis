using System.Net;
using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.Extensions.Options;
using Tennis.Domain.Configuration;
using Tennis.Domain.Exceptions;
using Tennis.Infrastructure.Services.Contract;
using File = Tennis.Infrastructure.Dto.File;

namespace Tennis.Infrastructure.Services.Implementation;

public class StorageService : IStorageService
{
    private readonly IAmazonS3 _amazonS3;
    private readonly ApplicationConfiguration _configuration;

    public StorageService(
        IAmazonS3 amazonS3,
        IOptions<ApplicationConfiguration> options
    )
    {
        _amazonS3 = amazonS3;
        _configuration = options.Value;
    }
    
    public async Task<Uri> Save(File file)
    {
        if (string.IsNullOrWhiteSpace(file.Name))
            throw new OperationException(ExceptionCodes.FileNotUploaded, "File has no name");
        var request = new PutObjectRequest
        {
            InputStream = file.Content,
            ContentType = file.Type,
            BucketName = _configuration.S3BucketName,
            Key = file.Name,
            CannedACL = S3CannedACL.PublicRead
        };
        var response = await _amazonS3.PutObjectAsync(request);
        if (response.HttpStatusCode != HttpStatusCode.OK && response.HttpStatusCode != HttpStatusCode.NoContent)
            throw new OperationException(
                ExceptionCodes.FileNotUploaded,
                $"File upload ({file.Name}) returned {response.HttpStatusCode}, but expected OK or No Content");
        return new Uri($"https://{_configuration.S3BucketName}.s3.amazonaws.com/{file.Name}");
    }

    public async Task Delete(string fileName)
    {
        if (string.IsNullOrWhiteSpace(fileName))
            throw new OperationException(ExceptionCodes.FileNotDeleted, "File has no name");
        var response = await _amazonS3.DeleteObjectAsync(_configuration.S3BucketName, fileName);
        if (response.HttpStatusCode != HttpStatusCode.OK && response.HttpStatusCode != HttpStatusCode.NoContent)
            throw new OperationException(
                ExceptionCodes.FileNotDeleted,
                $"File delete ({fileName}) returned {response.HttpStatusCode}, but expected OK or No Content");
    }
}