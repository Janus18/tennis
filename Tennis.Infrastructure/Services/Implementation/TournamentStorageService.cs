using Tennis.Domain.Tournament;
using Tennis.Infrastructure.Services.Contract;

namespace Tennis.Infrastructure.Services.Implementation;

public class TournamentStorageService : ITournamentStorageService
{
    private readonly IStorageService storageService;

    public TournamentStorageService(IStorageService storageService)
    {
        this.storageService = storageService;
    }

    public Task Delete(Tournament tournament)
        => storageService.Delete(FileName(tournament));

    public Task<Uri> Save(Tournament tournament, Dto.File file)
        => storageService.Save(file.WithName(FileName(tournament)));
    
    private static string FileName(Tournament tournament)
        => $"Tournaments/{tournament.Name.Value.Replace(' ', '-')}/logo";
}
