using Tennis.Domain.Player;
using Tennis.Infrastructure.Services.Contract;

namespace Tennis.Infrastructure.Services.Implementation;

public class PlayerStorageService : IPlayerStorageService
{
    private readonly IStorageService storageService;

    public PlayerStorageService(IStorageService storageService)
    {
        this.storageService = storageService;
    }

    public Task Delete(Player player)
        => storageService.Delete(FileName(player));

    public Task<Uri> Save(Player player, Dto.File file)
        => storageService.Save(file.WithName(FileName(player)));

    private static string FileName(Player player)
        => $"Players/{player.Name.Value.Replace(' ', '-')}/image";
}
