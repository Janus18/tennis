using MediatR;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Tennis.Domain.Configuration;
using Tennis.Domain.Contract.DataAccess;
using Tennis.Domain.Player;
using Tennis.Domain.Player.Specifications;
using Tennis.Infrastructure.Services.Contract;

namespace Tennis.Infrastructure.Commands.AddPlayer;

public class AddPlayerHandler : IRequestHandler<AddPlayerCommand, AddPlayerResult>
{
    private readonly IPlayerRepository _playerRepository;
    private readonly IPlayerStorageService _storageService;
    private readonly IOptions<ApplicationConfiguration> _options;
    private readonly ILogger<AddPlayerHandler> _logger;

    public AddPlayerHandler(
        IPlayerRepository playerRepository,
        IPlayerStorageService storageService,
        IOptions<ApplicationConfiguration> options,
        ILogger<AddPlayerHandler> logger)
    {
        _playerRepository = playerRepository;
        _storageService = storageService;
        _options = options;
        _logger = logger;
    }

    public async Task<AddPlayerResult> Handle(AddPlayerCommand request, CancellationToken cancellationToken)
    {
        Player player = await BuildPlayer(request);
        player.ImageUri = await SaveImage(player, request);
        await TrySavePlayer(player);
        return new AddPlayerResult { Id = player.Id };
    }

    private async Task<Player> BuildPlayer(AddPlayerCommand request)
    {
        var spec = new PlayerSpec(_playerRepository, _options)
        {
            Gender = request.Player.Gender,
            Name = request.Player.Name,
            Reaction = request.Player.Reaction,
            Speed = request.Player.Speed,
            Strength = request.Player.Strength
        };
        var result = await spec.Apply();
        return result.Unpack();
    }

    private async Task TrySavePlayer(Player player)
    {
        try
        {
            await _playerRepository.AddAsync(player);
        }
        catch (Exception)
        {
            await DeleteImage(player);
            throw;
        }
    }

    private async Task<Uri?> SaveImage(Player player, AddPlayerCommand request)
    {
        if (request.Image is null)
            return null;
        return await _storageService.Save(player, request.Image);
    }
    
    private async Task DeleteImage(Player player)
    {
        if (player.ImageUri is null)
            return;
        try
        {
            await _storageService.Delete(player);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Image could not be deleted");
        }
    }
}
