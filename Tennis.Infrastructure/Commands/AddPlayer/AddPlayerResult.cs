namespace Tennis.Infrastructure.Commands.AddPlayer;

public class AddPlayerResult
{
    public Guid Id { get; init; }
}
