using Tennis.Domain.Player;

namespace Tennis.Infrastructure.Commands.AddPlayer;

public class AddPlayerInput
{
    public string Name { get; set; } = "";

    public int Strength { get; set; }

    public int Speed { get; set; }

    public int Reaction { get; set; }

    public Gender Gender { get; set; }
}
