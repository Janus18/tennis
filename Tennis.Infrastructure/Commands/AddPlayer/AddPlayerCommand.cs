using MediatR;

namespace Tennis.Infrastructure.Commands.AddPlayer;

public class AddPlayerCommand : IRequest<AddPlayerResult>
{
    public AddPlayerInput Player { get; }
    public Dto.File? Image { get; }

    public AddPlayerCommand(AddPlayerInput player, Dto.File? image)
    {
        Player = player;
        Image = image;
    }
}
