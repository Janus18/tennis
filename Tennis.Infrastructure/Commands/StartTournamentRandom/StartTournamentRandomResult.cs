namespace Tennis.Infrastructure.Commands.StartTournamentRandom;

public class StartTournamentRandomResult
{
    public Guid Id { get; init; }
}