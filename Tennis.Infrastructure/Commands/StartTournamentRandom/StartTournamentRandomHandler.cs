using System.Linq.Expressions;
using MediatR;
using Microsoft.Extensions.Options;
using Tennis.Domain.Configuration;
using Tennis.Domain.Contract.DataAccess;
using Tennis.Domain.Exceptions;
using Tennis.Domain.Tournament;
using Tennis.Domain.Tournament.Specifications;

namespace Tennis.Infrastructure.Commands.StartTournamentRandom;

public class StartTournamentRandomHandler : IRequestHandler<StartTournamentRandomCommand, StartTournamentRandomResult>
{
    private readonly ITournamentRepository _tournamentRepository;
    private readonly ITournamentInstanceRepository _tournamentInstanceRepository;
    private readonly IPlayerRepository _playerRepository;
    private readonly IOptions<ApplicationConfiguration> _options;

    public StartTournamentRandomHandler(
        ITournamentRepository tournamentRepository,
        ITournamentInstanceRepository tournamentInstanceRepository,
        IPlayerRepository playerRepository,
        IOptions<ApplicationConfiguration> options)
    {
        _tournamentRepository = tournamentRepository;
        _tournamentInstanceRepository = tournamentInstanceRepository;
        _playerRepository = playerRepository;
        _options = options;
    }
    
    public async Task<StartTournamentRandomResult> Handle(StartTournamentRandomCommand request, CancellationToken cancellationToken)
    {
        var tournament = await _tournamentRepository.Get(t => t.Id == request.TournamentId);
        if (tournament is null)
            throw new ValidationException(
                ExceptionCodes.TournamentNotFound,
                "The indicated tournament was not found");
        var spec = new TournamentInstanceSpec(_tournamentRepository, _tournamentInstanceRepository, _playerRepository, _options)
        {
            Tournament = tournament,
            Gender = request.Gender,
            PlayersIds = await FindPlayers(request, tournament),
            Year = request.Year,
        };
        var result = await spec.Apply();
        var tournamentInstance = result.Unpack();
        await _tournamentInstanceRepository.AddAsync(tournamentInstance);
        return new StartTournamentRandomResult { Id = tournamentInstance.Id };
    }

    private async Task<IList<Guid>> FindPlayers(StartTournamentRandomCommand request, Tournament tournament)
    {
        Expression<Func<Domain.Player.Player, bool>> predicate = request.Gender.HasValue
                    ? ((p) => p.Gender == request.Gender)
                    : ((p) => true);
        var players = await _playerRepository.GetMany(predicate, page: new Domain.ValueObjects.Page(0, (ushort)tournament.SinglesDraw));
        if (players.Count() < tournament.SinglesDraw)
        {
            throw new ValidationException(
                ExceptionCodes.NotEnoughPlayers,
                $"Only {players.Count()} players were found to play the tournament, but the draw is {tournament.SinglesDraw}"
            );
        }
        return players.Select(p => p.Id).ToList();
    }
}