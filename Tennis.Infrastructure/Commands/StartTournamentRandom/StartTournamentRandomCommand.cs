using MediatR;
using Tennis.Domain.Player;

namespace Tennis.Infrastructure.Commands.StartTournamentRandom;

public class StartTournamentRandomCommand : IRequest<StartTournamentRandomResult>
{
    public Guid TournamentId { get; }

    public Gender? Gender { get; set; }

    public int Year { get; set; }

    public StartTournamentRandomCommand(Guid tournamentId, Gender? gender, int year)
    {
        TournamentId = tournamentId;
        Gender = gender;
        Year = year;
    }
}