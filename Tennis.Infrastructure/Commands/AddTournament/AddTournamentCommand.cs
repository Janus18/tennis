using MediatR;
using File = Tennis.Infrastructure.Dto.File;

namespace Tennis.Infrastructure.Commands.AddTournament;

public class AddTournamentCommand : IRequest<AddTournamentOutput>
{
    public AddTournamentInput Input { get; }
    public File? Image { get; }

    public AddTournamentCommand(AddTournamentInput input, File? image)
    {
        Input = input;
        Image = image;
    }
}