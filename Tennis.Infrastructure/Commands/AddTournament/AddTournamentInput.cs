using NodaTime;

namespace Tennis.Infrastructure.Commands.AddTournament;

public class AddTournamentInput
{
    public string Name { get; set; } = "";
    public string SurfaceType { get; set; } = "";
    public string Location { get; set; } = "";
    public uint SinglesDraw { get; set; }
    public int StartDateDay { get; set; }
    public int StartDateMonth { get; set; }
}