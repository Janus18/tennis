using MediatR;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Tennis.Domain.Configuration;
using Tennis.Domain.Contract.DataAccess;
using Tennis.Domain.Tournament;
using Tennis.Domain.Tournament.Specifications;
using Tennis.Infrastructure.Services.Contract;

namespace Tennis.Infrastructure.Commands.AddTournament;

public class AddTournamentHandler : IRequestHandler<AddTournamentCommand, AddTournamentOutput>
{
    private readonly ITournamentRepository _tournamentRepository;
    private readonly ITournamentStorageService _storageService;
    private readonly IOptions<ApplicationConfiguration> _options;
    private readonly ILogger<AddTournamentHandler> _logger;

    public AddTournamentHandler(
        ITournamentRepository tournamentRepository,
        ITournamentStorageService storageService,
        IOptions<ApplicationConfiguration> options,
        ILogger<AddTournamentHandler> logger)
    {
        _tournamentRepository = tournamentRepository;
        _storageService = storageService;
        _options = options;
        _logger = logger;
    }
    
    public async Task<AddTournamentOutput> Handle(AddTournamentCommand request, CancellationToken cancellationToken)
    {
        var tournament = await BuildTournament(request);
        tournament.ImageUri = await SaveImage(tournament, request);
        await TrySaveTournament(tournament);
        return new AddTournamentOutput { Id = tournament.Id };
    }

    private async Task<Tournament> BuildTournament(AddTournamentCommand request)
    {
        var spec = new TournamentSpec(_tournamentRepository, _options)
        {
            Location = request.Input.Location,
            Name = request.Input.Name,
            SinglesDraw = request.Input.SinglesDraw,
            StartDateDay = request.Input.StartDateDay,
            StartDateMonth = request.Input.StartDateMonth,
            SurfaceType = request.Input.SurfaceType
        };
        var specResult = await spec.Apply();
        var tournament = specResult.Unpack();
        return tournament;
    }

    private async Task TrySaveTournament(Tournament tournament)
    {
        try
        {
            await _tournamentRepository.AddAsync(tournament);
        }
        catch (Exception)
        {
            await DeleteImage(tournament);
            throw;
        }
    }

    private async Task<Uri?> SaveImage(Tournament tournament, AddTournamentCommand command)
    {
        if (command.Image is null)
            return null;
        return await _storageService.Save(tournament, command.Image);
    }
    
    private async Task DeleteImage(Tournament tournament)
    {
        if (tournament.ImageUri is null)
            return;
        try
        {
            await _storageService.Delete(tournament);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Image could not be deleted");
        }
    }
}