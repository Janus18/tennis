namespace Tennis.Infrastructure.Commands.AddTournament;

public class AddTournamentOutput
{
    public Guid Id { get; init; }
}