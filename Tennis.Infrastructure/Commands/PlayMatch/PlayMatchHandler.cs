using MediatR;
using Tennis.Domain.Contract.DataAccess;
using Tennis.Domain.Contract.Services;
using Tennis.Domain.Exceptions;

namespace Tennis.Infrastructure.Commands.PlayMatch;

public class PlayMatchHandler : IRequestHandler<PlayMatchCommand, PlayMatchResult>
{
    private readonly ITournamentInstanceRepository _tournamentInstanceRepository;
    private readonly IRandomService _randomService;

    public PlayMatchHandler(
        ITournamentInstanceRepository tournamentInstanceRepository,
        IRandomService randomService
    )
    {
        _tournamentInstanceRepository = tournamentInstanceRepository;
        _randomService = randomService;
    }
    
    public async Task<PlayMatchResult> Handle(PlayMatchCommand request, CancellationToken cancellationToken)
    {
        var tournament = await _tournamentInstanceRepository.Get(request.TournamentInstanceId);
        if (tournament is null)
            throw new ValidationException(
                ExceptionCodes.TournamentInstanceNotFound,
                $"No Tournament instance was found with ID {request.TournamentInstanceId}");
        var match = tournament.Matches.FirstOrDefault(m => m.Id == request.MatchId);
        if (match is null)
            throw new ValidationException(
                ExceptionCodes.MatchNotFound,
                $"No Match was found with ID {request.MatchId}");
        if (!match.PlayersAreReady)
            throw new ValidationException(
                ExceptionCodes.MatchNotPlayable,
                "This match players are not ready yet.");
        var winner = tournament.PlayMatch(match, _randomService);
        await _tournamentInstanceRepository.UpdateAsync(tournament);
        return new PlayMatchResult(request.MatchId, new PlayMatchResultWinner(winner.Id, winner.Name.Value));
    }
}