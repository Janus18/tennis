using MediatR;

namespace Tennis.Infrastructure.Commands.PlayMatch;

public class PlayMatchCommand : IRequest<PlayMatchResult>
{
    public Guid TournamentInstanceId { get; }
    public Guid MatchId { get; }

    public PlayMatchCommand(Guid tournamentInstanceId, Guid matchId)
    {
        TournamentInstanceId = tournamentInstanceId;
        MatchId = matchId;
    }
}