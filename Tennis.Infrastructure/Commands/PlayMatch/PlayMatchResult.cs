namespace Tennis.Infrastructure.Commands.PlayMatch;

public record PlayMatchResult(Guid Id, PlayMatchResultWinner Winner);

public record PlayMatchResultWinner(Guid Id, string Name);
