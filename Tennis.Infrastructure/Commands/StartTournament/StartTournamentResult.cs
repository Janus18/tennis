namespace Tennis.Infrastructure.Commands.StartTournament;

public class StartTournamentResult
{
    public Guid Id { get; init; }
}