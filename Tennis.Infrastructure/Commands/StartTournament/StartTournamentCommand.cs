using MediatR;

namespace Tennis.Infrastructure.Commands.StartTournament;

public class StartTournamentCommand : IRequest<StartTournamentResult>
{
    public StartTournamentInput Input { get; }

    public StartTournamentCommand(StartTournamentInput input)
    {
        Input = input;
    }
}