using Tennis.Domain.Player;

namespace Tennis.Infrastructure.Commands.StartTournament;

public class StartTournamentInput
{
    public Guid TournamentId { get; set; }

    public Gender? Gender { get; set; }

    public int Year { get; set; }

    public IList<Guid> Players { get; set; } = new List<Guid>();
}