using MediatR;
using Microsoft.Extensions.Options;
using Tennis.Domain.Configuration;
using Tennis.Domain.Contract.DataAccess;
using Tennis.Domain.Exceptions;
using Tennis.Domain.Tournament.Specifications;

namespace Tennis.Infrastructure.Commands.StartTournament;

public class StartTournamentHandler : IRequestHandler<StartTournamentCommand, StartTournamentResult>
{
    private readonly ITournamentRepository _tournamentRepository;
    private readonly ITournamentInstanceRepository _tournamentInstanceRepository;
    private readonly IPlayerRepository _playerRepository;
    private readonly IOptions<ApplicationConfiguration> _options;

    public StartTournamentHandler(
        ITournamentRepository tournamentRepository,
        ITournamentInstanceRepository tournamentInstanceRepository,
        IPlayerRepository playerRepository,
        IOptions<ApplicationConfiguration> options)
    {
        _tournamentRepository = tournamentRepository;
        _tournamentInstanceRepository = tournamentInstanceRepository;
        _playerRepository = playerRepository;
        _options = options;
    }
    
    public async Task<StartTournamentResult> Handle(StartTournamentCommand request, CancellationToken cancellationToken)
    {
        var tournament = await _tournamentRepository.Get(t => t.Id == request.Input.TournamentId);
        if (tournament is null)
            throw new ValidationException(
                ExceptionCodes.TournamentNotFound,
                "The indicated tournament was not found");
        var spec = new TournamentInstanceSpec(_tournamentRepository, _tournamentInstanceRepository, _playerRepository, _options)
        {
            Tournament = tournament,
            Gender = request.Input.Gender,
            PlayersIds = request.Input.Players,
            Year = request.Input.Year,
        };
        var result = await spec.Apply();
        var tournamentInstance = result.Unpack();
        await _tournamentInstanceRepository.AddAsync(tournamentInstance);
        return new StartTournamentResult { Id = tournamentInstance.Id };
    }
}