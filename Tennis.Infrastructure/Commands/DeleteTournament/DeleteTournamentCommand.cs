using MediatR;

namespace Tennis.Infrastructure.Commands.DeleteTournament;

public class DeleteTournamentCommand : IRequest
{
    public Guid TournamentId { get; }

    public DeleteTournamentCommand(Guid tournamentId)
    {
        TournamentId = tournamentId;
    }
}