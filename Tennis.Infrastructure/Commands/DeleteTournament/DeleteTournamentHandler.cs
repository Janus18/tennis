using MediatR;
using Microsoft.Extensions.Logging;
using Tennis.Domain.Contract.DataAccess;
using Tennis.Domain.Exceptions;
using Tennis.Domain.Tournament;
using Tennis.Infrastructure.Services.Contract;

namespace Tennis.Infrastructure.Commands.DeleteTournament;

public class DeleteTournamentHandler : IRequestHandler<DeleteTournamentCommand>
{
    private readonly ITournamentRepository tournamentRepository;
    private readonly ITournamentStorageService storageService;
    private readonly ILogger<DeleteTournamentHandler> logger;

    public DeleteTournamentHandler(
        ITournamentRepository tournamentRepository,
        ITournamentStorageService storageService,
        ILogger<DeleteTournamentHandler> logger
    )
    {
        this.tournamentRepository = tournamentRepository;
        this.storageService = storageService;
        this.logger = logger;
    }

    public async Task Handle(DeleteTournamentCommand request, CancellationToken cancellationToken)
    {
        var tournament = await tournamentRepository.Get(t => t.Id == request.TournamentId)
            ?? throw new ValidationException(ExceptionCodes.TournamentNotFound, $"No tournament found with ID {request.TournamentId}");
        await tournamentRepository.DeleteAsync(tournament);
        await DeleteImage(tournament);
    }

    private async Task DeleteImage(Tournament tournament)
    {
        if (tournament.ImageUri is null)
            return;
        try
        {
            await storageService.Delete(tournament);
        }
        catch (Exception exception)
        {
            logger.LogError(exception, "Error deleting tournament image. Tournament ID: {ID}", tournament.Id);
        }
    }
}