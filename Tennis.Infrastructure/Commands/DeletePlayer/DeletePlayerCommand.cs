using MediatR;

namespace Tennis.Infrastructure.Commands;

public class DeletePlayerCommand : IRequest
{
    public Guid PlayerId { get; }

    public DeletePlayerCommand(Guid playerId)
    {
        PlayerId = playerId;
    }
}
