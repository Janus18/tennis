using MediatR;
using Microsoft.Extensions.Logging;
using Tennis.Domain.Contract.DataAccess;
using Tennis.Domain.Exceptions;
using Tennis.Domain.Player;
using Tennis.Infrastructure.Services.Contract;

namespace Tennis.Infrastructure.Commands;

public class DeletePlayerHandler : IRequestHandler<DeletePlayerCommand>
{
    private readonly IPlayerRepository playerRepository;
    private readonly IPlayerStorageService storageService;
    private readonly ILogger<DeletePlayerHandler> logger;

    public DeletePlayerHandler(
        IPlayerRepository playerRepository,
        IPlayerStorageService storageService,
        ILogger<DeletePlayerHandler> logger
    )
    {
        this.playerRepository = playerRepository;
        this.storageService = storageService;
        this.logger = logger;
    }

    public async Task Handle(DeletePlayerCommand request, CancellationToken cancellationToken)
    {
        var player = await playerRepository.Get(p => p.Id == request.PlayerId)
            ?? throw new ValidationException(ExceptionCodes.PlayerNotFound, $"No Player found with ID {request.PlayerId}");
        await playerRepository.DeleteAsync(player);
        await DeleteImage(player);
    }

    private async Task DeleteImage(Player player)
    {
        if (player.ImageUri is null)
            return;
        try
        {
            await storageService.Delete(player);
        }
        catch (Exception exception)
        {
            logger.LogError(exception, "Error deleting player image. Player ID: {ID}", player.Id);
        }
    }
}
