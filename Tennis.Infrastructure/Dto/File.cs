namespace Tennis.Infrastructure.Dto;

public class File
{
    public Stream Content { get; }
    public string Type { get; }
    public string? Name { get; }

    public File(Stream content, string type, string? name = null)
    {
        Content = content;
        Type = type;
        Name = name;
    }

    public File WithName(string name) => new File(Content, Type, name);
}