namespace Tennis.Infrastructure.Dto;

public class PlayerOutputDto
{
    public Guid Id { get; init; }

    public string Name { get; init; } = "";

    public int Strength { get; init; }

    public int Speed { get; init; }

    public int Reaction { get; init; }

    public string Gender { get; init; } = "";
    
    public string? ImageUri { get; init; }
}