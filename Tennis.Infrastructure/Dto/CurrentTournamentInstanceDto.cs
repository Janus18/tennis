using NodaTime;
using Tennis.Domain.Match;
using Tennis.Domain.Tournament;

namespace Tennis.Infrastructure.Dto;

public record CurrentTournamentInstanceDto
{
    public Guid Id { get; init; }

    public Guid TournamentId { get; init; }
    
    public string Gender { get; init; } = "";

    public LocalDate StartDate { get; init; }
    
    public string Name { get; init; } = "";

    public string SurfaceType { get; init; } = "";
    
    public string Location { get; init; } = "";
    
    public uint SinglesDraw { get; init; }
    
    public string? ImageUri { get; init; }

    public CurrentTournamentInstanceDtoMatch NextMatch { get; init; } = null!;

    public CurrentTournamentInstanceDto(TournamentInstance t)
    {
        Id = t.Id;
        TournamentId = t.Tournament.Id;
        StartDate = t.StartDate;
        Gender = t.DisplayGender;
        ImageUri = t.Tournament.DisplayImageUri;
        Location = t.Tournament.Location;
        Name = t.Tournament.Name.Value;
        SinglesDraw = t.Tournament.SinglesDraw;
        SurfaceType = t.Tournament.SurfaceType.ToString();
        NextMatch = new CurrentTournamentInstanceDtoMatch(t
            .Matches
            .Where(m => m is { Winner: null, Player1: not null, Player2: not null })
            .OrderBy(m => m.Date)
            .First()
        );
    }
}

public record CurrentTournamentInstanceDtoMatch
{
    public LocalDate Date { get; init; }
    
    public TournamentPlayerDto Player1 { get; init; }
    
    public TournamentPlayerDto Player2 { get; init; }

    public CurrentTournamentInstanceDtoMatch(Match match)
    {
        Date = match.Date;
        Player1 = new TournamentPlayerDto(match.Player1!.Id, match.Player1.Name.Value, match.Player1.ImageUri);
        Player2 = new TournamentPlayerDto(match.Player2!.Id, match.Player2.Name.Value, match.Player2.ImageUri);
    }
}
