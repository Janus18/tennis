namespace Tennis.Infrastructure.Dto;

public record TournamentPlayerDto
{
    public Guid Id { get; init; }

    public string Name { get; init; } = "";
    
    public string? ImageUri { get; init; }

    public TournamentPlayerDto(Guid id, string name, Uri? imageUri)
    {
        Id = id;
        Name = name;
        ImageUri = imageUri?.AbsoluteUri;
    }
}
