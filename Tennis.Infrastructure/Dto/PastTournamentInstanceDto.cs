using NodaTime;

namespace Tennis.Infrastructure.Dto;

public record PastTournamentInstanceDto
{
    public Guid Id { get; init; }

    public Guid TournamentId { get; init; }
    
    public string Gender { get; init; } = "";

    public LocalDate StartDate { get; init; }
    
    public LocalDate EndDate { get; init; }
    
    public string Name { get; init; } = "";

    public string SurfaceType { get; init; } = "";
    
    public string Location { get; init; } = "";
    
    public uint SinglesDraw { get; init; }
    
    public string? ImageUri { get; init; }

    public TournamentPlayerDto Winner { get; init; } = null!;
}
