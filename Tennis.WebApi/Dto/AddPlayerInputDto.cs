using Tennis.Infrastructure.Commands.AddPlayer;

namespace Tennis.WebApi.Dto;

public class AddPlayerInputDto : AddPlayerInput
{
    public IFormFile? Image { get; set; }

    public Infrastructure.Dto.File? ImageFile => Image is null
        ? null
        : new Infrastructure.Dto.File(Image.OpenReadStream(), Image.ContentType);
}