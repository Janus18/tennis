using Tennis.Infrastructure.Commands.AddTournament;
using File = Tennis.Infrastructure.Dto.File;

namespace Tennis.WebApi.Dto;

public class AddTournamentInputDto : AddTournamentInput
{
    public IFormFile? Image { get; set; }

    public File? ImageFile => Image is null ? null : new File(Image.OpenReadStream(), Image.ContentType);
}