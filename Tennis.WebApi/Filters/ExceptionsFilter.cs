using System.Text.Json;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Tennis.Domain.Exceptions;
using Tennis.Domain.Specifications.Implementation;

namespace Tennis.WebApi.Filters;

public class ExceptionsFilter : IExceptionFilter
{
    private readonly IHostEnvironment _hostEnvironment;
    private readonly ILogger<ExceptionsFilter> _logger;

    public ExceptionsFilter(
        IHostEnvironment hostEnvironment,
        ILogger<ExceptionsFilter> logger
    )
    {
        _hostEnvironment = hostEnvironment;
        _logger = logger;
    }
    
    public void OnException(ExceptionContext context)
    {
        switch (context.Exception)
        {
            case SpecificationException specificationException:
                SpecificationExceptionHandler(context, specificationException);
                break;
            case ValidationException notFoundException:
                ValidationExceptionHandler(context, notFoundException);
                break;
            default:
                _logger.LogError(context.Exception, "Unexpected exception");
                context.Result = _hostEnvironment.IsProduction()
                    ? new StatusCodeResult(500)
                    : new ContentResult { StatusCode = 500, Content = context.Exception.Message };
                break;
        }
    }

    private void SpecificationExceptionHandler(ExceptionContext context, SpecificationException specificationException)
    {
        _logger.LogWarning(specificationException, "Specification exception");
        context.Result = new ObjectResult(specificationException.Errors)
        {
            StatusCode = 400
        };
        context.ExceptionHandled = true;
    }

    private void ValidationExceptionHandler(ExceptionContext context, ValidationException exception)
    {
        _logger.LogWarning(exception, "Validation exception");
        context.Result = new ContentResult()
        {
            StatusCode = 400,
            Content = JsonSerializer.Serialize(new
            {
                error = new { code = exception.Code.ToString(), message = exception.Message }
            }),
            ContentType = "application/json"
        };
        context.ExceptionHandled = true;
    }
}