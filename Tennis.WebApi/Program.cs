using Microsoft.EntityFrameworkCore;
using NodaTime;
using NodaTime.Serialization.SystemTextJson;
using Tennis.Domain.Configuration;
using Tennis.Infrastructure;
using Tennis.Infrastructure.DataAccess;
using Tennis.WebApi.ConfigurationProviders;
using Tennis.WebApi.Filters;

var builder = WebApplication.CreateBuilder(args);

builder.Configuration.AddJsonFile("appsettings.json", false);
builder.Configuration.AddUserSecrets(typeof(Program).Assembly, true);
if (builder.Environment.IsProduction())
{
    builder.Configuration.AddAWSSecretsManager("Tennis", "us-east-1");
}

builder.Services.AddInfrastructure(builder.Configuration);
builder.Services.Configure<ApplicationConfiguration>(builder.Configuration);
builder.Services.AddSingleton<IConfiguration>(builder.Configuration);

builder.Services.AddControllers(options => { options.Filters.Add<ExceptionsFilter>(); })
    .AddJsonOptions(opts => opts.JsonSerializerOptions.ConfigureForNodaTime(DateTimeZoneProviders.Tzdb));
builder.Services.AddSwaggerGen();

var app = builder.Build();

using (var scope = app.Services.CreateScope())
{
    var context = scope.ServiceProvider.GetRequiredService<Context>();
    context.Database.Migrate();
}

app.UseSwagger();
app.UseSwaggerUI();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
