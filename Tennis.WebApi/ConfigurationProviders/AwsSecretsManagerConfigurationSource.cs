namespace Tennis.WebApi.ConfigurationProviders;

public class AwsSecretsManagerConfigurationSource : IConfigurationSource
{
    private readonly string _secretName;
    private readonly string _region;

    public AwsSecretsManagerConfigurationSource(string secretName, string region)
    {
        _secretName = secretName;
        _region = region;
    }

    public IConfigurationProvider Build(IConfigurationBuilder builder)
        => new AwsSecretsManagerProvider(_secretName, _region);
}
