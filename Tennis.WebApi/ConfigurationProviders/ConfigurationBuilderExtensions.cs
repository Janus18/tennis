namespace Tennis.WebApi.ConfigurationProviders;

public static class ConfigurationBuilderExtensions
{
    public static IConfigurationBuilder AddAWSSecretsManager(this IConfigurationBuilder builder, string secretName, string region)
        => builder.Add(new AwsSecretsManagerConfigurationSource(secretName, region));
}
