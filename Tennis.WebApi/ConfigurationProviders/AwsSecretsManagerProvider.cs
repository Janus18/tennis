using Amazon;
using Amazon.SecretsManager;
using Amazon.SecretsManager.Model;

namespace Tennis.WebApi.ConfigurationProviders;

public class AwsSecretsManagerProvider : ConfigurationProvider
{
    private readonly string _secretName;
    private readonly string _region;

    public AwsSecretsManagerProvider(string secretName, string region)
    {
        _secretName = secretName;
        _region = region;
    }

    public override void Load()
    {
        var secret = GetSecret().Result;
        var data =  System.Text.Json.JsonSerializer.Deserialize<Dictionary<string, string?>>(secret)
            ?? new Dictionary<string, string?>();
        Data = data;
    }

    private async Task<string> GetSecret()
    {
        IAmazonSecretsManager client = new AmazonSecretsManagerClient(RegionEndpoint.GetBySystemName(_region));
        var request = new GetSecretValueRequest { SecretId = _secretName };
        var response = await client.GetSecretValueAsync(request);
        return response.SecretString;
    }
}