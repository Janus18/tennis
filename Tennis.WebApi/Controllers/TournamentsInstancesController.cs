using MediatR;
using Microsoft.AspNetCore.Mvc;
using Tennis.Domain.Player;
using Tennis.Infrastructure.Commands.PlayMatch;
using Tennis.Infrastructure.Commands.StartTournament;
using Tennis.Infrastructure.Commands.StartTournamentRandom;
using Tennis.Infrastructure.Queries.GetTournamentInstance;
using Tennis.Infrastructure.Queries.TournamentsInstancesFeed;

namespace Tennis.WebApi.Controllers;

[ApiController]
[Route("[controller]")]
public class TournamentsInstancesController : ControllerBase
{
    private readonly IMediator _mediator;

    public TournamentsInstancesController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpPost]
    public async Task<IActionResult> StartInstance(StartTournamentInput input)
    {
        var command = new StartTournamentCommand(input);
        var result = await _mediator.Send(command);
        return CreatedAtAction(nameof(ListInstances), new { id = result.Id }, result);
    }

    [HttpPost("random")]
    public async Task<IActionResult> StartRandomInstance([FromQuery] Guid tournamentId, [FromQuery] int year, [FromQuery] Gender? gender)
    {
        var command = new StartTournamentRandomCommand(tournamentId, gender, year);
        var result = await _mediator.Send(command);
        return CreatedAtAction(nameof(ListInstances), new { id = result.Id }, result);
    }

    [HttpGet("feed")]
    public async Task<IActionResult> ListInstances(
        [FromQuery] ushort currentCount,
        [FromQuery] ushort pastCount,
        [FromQuery] ushort futureCount
    ) {
        var query = new TournamentsInstancesFeedQuery(currentCount, pastCount, futureCount);
        var result = await _mediator.Send(query);
        return Ok(result);
    }

    [HttpGet("{id:guid}")]
    public async Task<IActionResult> GetInstance([FromRoute] Guid tournamentId, [FromRoute] Guid id)
    {
        var query = new GetTournamentInstanceQuery(id);
        var result = await _mediator.Send(query);
        return result is null ? NotFound() : Ok(result);
    }

    [HttpPut("{tournamentInstanceId:guid}/matches/{matchId:guid}")]
    public async Task<IActionResult> PlayMatch([FromRoute] Guid tournamentInstanceId, [FromRoute] Guid matchId)
    {
        var command = new PlayMatchCommand(tournamentInstanceId, matchId);
        var result = await _mediator.Send(command);
        return Ok(result);
    }
}
