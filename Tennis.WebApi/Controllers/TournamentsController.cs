using MediatR;
using Microsoft.AspNetCore.Mvc;
using Tennis.Infrastructure.Commands.AddTournament;
using Tennis.Infrastructure.Queries.GetTournament;
using Tennis.Infrastructure.Queries.ListTournaments;
using Tennis.WebApi.Dto;
using Tennis.Infrastructure.Commands.DeleteTournament;

namespace Tennis.WebApi.Controllers;

[ApiController]
[Route("[controller]")]
public class TournamentsController : ControllerBase
{
    private readonly IMediator _mediator;
    
    public TournamentsController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpPost]
    public async Task<IActionResult> Add([FromForm] AddTournamentInputDto input)
    {
        var command = new AddTournamentCommand(input, input.ImageFile);
        var result = await _mediator.Send(command);
        return CreatedAtAction(nameof(Get), new { tournamentId = result.Id }, result);
    }

    [HttpGet("{tournamentId:guid}")]
    public async Task<IActionResult> Get(Guid tournamentId)
    {
        var query = new GetTournamentQuery(tournamentId);
        var result = await _mediator.Send(query);
        return result is null ? NotFound() : Ok(result);
    }

    [HttpDelete("{tournamentId:guid}")]
    public async Task<IActionResult> Delete(Guid tournamentId)
    {
        var command = new DeleteTournamentCommand(tournamentId);
        await _mediator.Send(command);
        return NoContent();
    }

    [HttpGet]
    public async Task<IActionResult> List([FromQuery] ushort page, [FromQuery] ushort count)
    {
        var query = new ListTournamentsQuery(count, page);
        var result = await _mediator.Send(query);
        return Ok(result);
    }
}
