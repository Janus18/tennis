using MediatR;
using Microsoft.AspNetCore.Mvc;
using Tennis.Domain.Player;
using Tennis.Infrastructure.Commands;
using Tennis.Infrastructure.Commands.AddPlayer;
using Tennis.Infrastructure.Queries.GetPlayer;
using Tennis.Infrastructure.Queries.ListPlayers;
using Tennis.WebApi.Dto;

namespace Tennis.WebApi.Controllers;

[ApiController]
[Route("[controller]")]
public class PlayersController : ControllerBase
{
    private readonly IMediator _mediator;

    public PlayersController(IMediator mediator)
    {
        this._mediator = mediator;
    }

    [HttpPost]
    public async Task<IActionResult> Create([FromForm] AddPlayerInputDto player)
    {
        var command = new AddPlayerCommand(player, player.ImageFile);
        var result = await _mediator.Send(command);
        return CreatedAtAction(nameof(Get), new { playerId = result.Id }, result);
    }

    [HttpGet]
    public async Task<IActionResult> List([FromQuery] ushort page, [FromQuery] ushort count, [FromQuery] Gender? gender = null, [FromQuery] string? name = null)
    {
        var query = new ListPlayersQuery(page, count, gender, name);
        var result = await _mediator.Send(query);
        return Ok(result);
    }

    [HttpGet("{playerId:guid}")]
    public async Task<IActionResult> Get([FromRoute] Guid playerId)
    {
        var query = new GetPlayerQuery(playerId);
        var player = await _mediator.Send(query);
        return player is null ? NotFound() : Ok(player);
    }

    [HttpDelete("{playerId:guid}")]
    public async Task<IActionResult> Delete([FromRoute] Guid playerId)
    {
        var command = new DeletePlayerCommand(playerId);
        await _mediator.Send(command);
        return NoContent();
    }
}
