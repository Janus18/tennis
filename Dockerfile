# syntax=docker/dockerfile:1
FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build-env
WORKDIR /app
    
COPY . ./
RUN dotnet restore Tennis.sln
RUN dotnet publish -c Release -o out
    
FROM mcr.microsoft.com/dotnet/aspnet:6.0
ARG ENVIRONMENT="Development"
ENV ASPNETCORE_ENVIRONMENT=${ENVIRONMENT}
WORKDIR /app
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "Tennis.WebApi.dll"]
