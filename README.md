### Simulator of Tennis tournaments  
The API will allow to simulate a tournament with all the indicated players, which must be loaded into the application before  .
Each match and the winner of the tournament will be persisted so you can query the simulation later.  
To run the project, first make sure to have a running SQL server instance and add the connection string to it. You can
use user secrets or environment strings. The string will look like this:

> ConnectionStrings:Database=Server=localhost,1433;Database=Tennis;User Id=sa;Password=******;

To run the application as a container, you can use the following line:
> docker run -d \
--add-host=host.docker.internal:host-gateway \
--name tennis \
-p 8080:80 \
-e "ConnectionStrings:Database=Server=host.docker.internal,1433;Database=Tennis;User Id=sa;Password=******;" \
tennis:latest

You can omit the *--add-host* flag if you're using Windows.