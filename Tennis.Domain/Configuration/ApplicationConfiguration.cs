namespace Tennis.Domain.Configuration;

public class ApplicationConfiguration
{
    public ushort MaxPlayersCount { get; set; }
    public ushort MaxConcurrentTournaments { get; set; }
    public ushort MaxTournaments { get; set; }
    public string S3BucketName { get; set; } = "";
    public ushort MaxMatchesPerDay { get; set; }
}
