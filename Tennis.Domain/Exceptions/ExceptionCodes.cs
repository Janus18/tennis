namespace Tennis.Domain.Exceptions;

public enum ExceptionCodes
{
    TournamentNotFound = 1,
    MatchNotFound = 2,
    MatchNotPlayable = 3,
    WrongDrawNumber = 4,
    WrongTournament = 5,
    TournamentInstanceNotFound = 6,
    FileNotUploaded = 7,
    FileNotDeleted = 8,
    PlayerNotFound = 9,
    NotEnoughPlayers = 10,
}