namespace Tennis.Domain.Exceptions;

public class ValidationException : ApplicationException
{
    public ExceptionCodes Code { get; }
    
    public ValidationException(ExceptionCodes code, string message) : base(message)
    {
        Code = code;
    }
}