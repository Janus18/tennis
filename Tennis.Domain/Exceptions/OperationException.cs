namespace Tennis.Domain.Exceptions;

public class OperationException : ApplicationException
{
    public ExceptionCodes Code { get; }

    public OperationException(ExceptionCodes code, string message) : base(message)
    {
        Code = code;
    }
}