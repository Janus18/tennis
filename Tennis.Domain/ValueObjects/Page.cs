namespace Tennis.Domain.ValueObjects;

public record struct Page(ushort Number, ushort Count);