using System.Text.RegularExpressions;
using Tennis.Domain.Exceptions;

namespace Tennis.Domain.ValueObjects;

public record NormalizedName
{
    public string Value { get; }

    public NormalizedName(string value)
    {
        if (!IsValid(value))
            throw new DomainException($"{value} is not a valid normalized name");
        Value = value;
    }

    public static bool IsValid(string value)
        => Regex.IsMatch(value, "^[a-zA-Z0-9 ]*$");
}
