namespace Tennis.Domain.ValueObjects;

public record WeightedValue<T>(T Value, int Weight);