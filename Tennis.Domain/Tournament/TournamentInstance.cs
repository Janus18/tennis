using NodaTime;
using Tennis.Domain.Contract;
using Tennis.Domain.Contract.Services;
using Tennis.Domain.Exceptions;
using Tennis.Domain.Player;
using Tennis.Domain.Tournament.Generators.Contract;

namespace Tennis.Domain.Tournament;

public class TournamentInstance : IEntity, IAggregate
{
    public Guid Id { get; private set; }

    public Gender? Gender { get; private set; }

    private readonly List<Match.Match> _matches = new();
    public IReadOnlyCollection<Match.Match> Matches => _matches.AsReadOnly();

    public LocalDate StartDate { get; private set; }
    
    public LocalDate EndDate { get; private set; }

    public Tournament Tournament { get; private set; } = null!;

    public Player.Player? Winner { get; private set; }

    private TournamentInstance() { }

    public string DisplayGender => Gender?.ToString() ?? "Mixed";

    private uint PlayersCount => (uint)_matches.Count(m => m.Round == 1) * 2;

    private Match.Match LastMatch => Matches.OrderByDescending(m => m.Round).First();

    public bool IsFinished => Winner != null;
    
    public TournamentInstance(Tournament tournament, Gender? gender, LocalDate start, IMatchCreationStrategy matchesGenerator)
    {
        Tournament = tournament;
        Gender = gender;
        StartDate = start;
        _matches = matchesGenerator.CreateMatches(start).ToList();
        if (PlayersCount != tournament.SinglesDraw)
            throw new ValidationException(
                ExceptionCodes.WrongDrawNumber,
                $"The expected number of players was {tournament.SinglesDraw}, but it is {PlayersCount}");
        EndDate = LastMatch.Date;
    }
    
    public Player.Player PlayMatch(Match.Match match, IRandomService randomService)
    {
        if (!Matches.Contains(match))
            throw new ArgumentException("Match is not from this tournament", nameof(match));
        var winner = match.Play(randomService);
        SetNextMatchPlayer(match);
        if (LastMatch.IsFinished)
            Winner = LastMatch.Winner;
        return winner;
    }

    private void SetNextMatchPlayer(Match.Match previousMatch)
    {
        if (previousMatch.Winner is null)
            throw new ArgumentException($"Match was not played yet. ID {previousMatch.Id}", nameof(previousMatch));
        if (NextMatch(previousMatch) is not { } nextMatch)
            return;
        nextMatch.AddPlayer(previousMatch);
    }

    private Match.Match? NextMatch(Match.Match match)
        => Matches.FirstOrDefault(m => 
            m.Round == match.NextRound &&
            m.MatchNumber == Math.Ceiling((decimal)match.MatchNumber / 2));
}
