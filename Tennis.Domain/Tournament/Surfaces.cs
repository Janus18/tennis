namespace Tennis.Domain.Tournament;

public enum Surfaces
{
    Acrylic = 1,
    ArtificialClay = 2,
    ArtificialGrass = 3,
    Asphalt = 4,
    Carpet = 5,
    Clay = 6,
    Concrete = 7,
    Grass = 8,
    Other = 9
}
