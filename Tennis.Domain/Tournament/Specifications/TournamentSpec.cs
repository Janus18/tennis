using FluentValidation;
using Microsoft.Extensions.Options;
using NodaTime;
using Tennis.Domain.Configuration;
using Tennis.Domain.Contract.DataAccess;
using Tennis.Domain.Extensions;
using Tennis.Domain.Specifications.Contract;
using Tennis.Domain.Specifications.Implementation;
using Tennis.Domain.ValueObjects;

namespace Tennis.Domain.Tournament.Specifications;

public class TournamentSpec : IAsyncSpecification<Tournament>
{
    private readonly ITournamentRepository _tournamentRepository;
    private readonly ApplicationConfiguration _configuration;
    
    public string Name { get; init; } = "";
    public string SurfaceType { get; init; } = "";
    public string Location { get; init; } = "";
    public int StartDateDay { get; init; }
    public int StartDateMonth { get; init; }
    public uint SinglesDraw { get; init; }

    public TournamentSpec(ITournamentRepository tournamentRepository, IOptions<ApplicationConfiguration> options)
    {
        _tournamentRepository = tournamentRepository;
        _configuration = options.Value;
    }
    
    public async Task<ISpecificationResult<Tournament>> Apply()
    {
        var errors = await Validate();
        if (errors.Any())
            return new SpecificationError<Tournament>(errors);
        var tournament = new Tournament(
            Name,
            Enum.Parse<Surfaces>(SurfaceType),
            Location,
            SinglesDraw,
            new AnnualDate(StartDateMonth, StartDateDay));
        return new SpecificationSuccess<Tournament>(tournament);
    }

    public async Task<IList<ValidationError>> Validate()
    {
        var validator = new TournamentSpecValidator(_tournamentRepository, _configuration);
        var result = await validator.ValidateAsync(this);
        return result.Errors?.Select(x => new ValidationError(x)).ToList() ??
               new List<ValidationError>();
    }

    private class TournamentSpecValidator : AbstractValidator<TournamentSpec>
    {
        private readonly ITournamentRepository _tournamentRepository;
        private readonly ApplicationConfiguration _configuration;

        public TournamentSpecValidator(
            ITournamentRepository tournamentRepository,
            ApplicationConfiguration configuration
        )
        {
            _tournamentRepository = tournamentRepository;
            _configuration = configuration;

            RuleFor(s => s)
                .MustAsync((spec, tkn) => TournamentsLimitNotReached())
                .WithName("Tournament")
                .WithMessage("Tournaments limit reached");
            
            RuleFor(s => s.SurfaceType)
                .Cascade(CascadeMode.Stop)
                .NotEmpty().WithMessage("The surface type is required")
                .Must(x => Enum.IsDefined(typeof(Surfaces), x)).WithMessage("Is not a valid surface");

            RuleFor(s => s.Name)
                .Cascade(CascadeMode.Stop)
                .NotEmpty().WithMessage("The name is required")
                .MaximumLength(100).WithMessage("The name cannot exceed 100 characters")
                .Must(cs => NormalizedName.IsValid(cs))
                    .WithMessage("The name must consist of letters, digits and spaces only")
                .MustAsync((x, tkn) => NameIsUnique(x))
                    .WithMessage("A tournament with the same name already exists");

            RuleFor(s => s.Location)
                .Cascade(CascadeMode.Stop)
                .NotEmpty().WithMessage("The location is required")
                .MaximumLength(200).WithMessage("The location cannot exceed 200 characters");

            RuleFor(s => s.SinglesDraw)
                .Cascade(CascadeMode.Stop)
                .LessThanOrEqualTo(128u).WithMessage("The draw cannot exceed 128 players")
                .Must(x => x.IsPowerOfTwo()).WithMessage("The draw must be a power of two");

            RuleFor(s => new { s.StartDateDay, s.StartDateMonth })
                .Must(s => BeValidAnnualDate(s.StartDateMonth, s.StartDateDay)).WithMessage("Day and month do not form a valid date");
        }

        private async Task<bool> NameIsUnique(string name)
            => !await _tournamentRepository.Exists(t => t.Name == new TournamentName(name));

        private async Task<bool> TournamentsLimitNotReached()
            => await _tournamentRepository.Count(t => true) < _configuration.MaxTournaments;

        private bool BeValidAnnualDate(int month, int day)
        {
            try
            {
                new AnnualDate(month, day);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}