using FluentValidation;
using Microsoft.Extensions.Options;
using Nito.AsyncEx;
using NodaTime;
using Tennis.Domain.Configuration;
using Tennis.Domain.Contract.DataAccess;
using Tennis.Domain.Player;
using Tennis.Domain.Specifications.Contract;
using Tennis.Domain.Specifications.Implementation;
using Tennis.Domain.Tournament.Generators.Implementation;

namespace Tennis.Domain.Tournament.Specifications;

public class TournamentInstanceSpec : IAsyncSpecification<TournamentInstance>
{
    private readonly ITournamentRepository _tournamentRepository;
    private readonly ITournamentInstanceRepository _tournamentInstanceRepository;
    private readonly IPlayerRepository _playerRepository;
    private readonly ApplicationConfiguration _applicationConfiguration;

    public Tournament? Tournament { get; init; }
    
    public Gender? Gender { get; init; }

    public int Year { get; init; }

    private readonly IList<Guid> _playersIds = new List<Guid>();
    private readonly AsyncLazy<IEnumerable<Player.Player>> _players =
        new(() => Task.FromResult<IEnumerable<Player.Player>>(new List<Player.Player>()));
    public IList<Guid> PlayersIds
    {
        get => _playersIds;
        init
        {
            _playersIds = value;
            _players = new AsyncLazy<IEnumerable<Player.Player>>(
                () => _playerRepository.GetMany(p => value.Contains(p.Id)));
        }
    }
    
    public TournamentInstanceSpec(
        ITournamentRepository tournamentRepository,
        ITournamentInstanceRepository tournamentInstanceRepository,
        IPlayerRepository playerRepository,
        IOptions<ApplicationConfiguration> options
    )
    {
        _tournamentRepository = tournamentRepository;
        _tournamentInstanceRepository = tournamentInstanceRepository;
        _playerRepository = playerRepository;
        _applicationConfiguration = options.Value;
    }

    public async Task<ISpecificationResult<TournamentInstance>> Apply()
    {
        var errors = await Validate();
        if (errors.Any())
            return new SpecificationError<TournamentInstance>(errors);
        var players = await _players;
        var matchesGenerator = new CommonMatchCreationStrategy(players.ToList(), _applicationConfiguration);
        var instance = new TournamentInstance(
            Tournament!,
            Gender,
            Tournament!.StartDate.InYear(Year),
            matchesGenerator);
        return new SpecificationSuccess<TournamentInstance>(instance);
    }

    public async Task<IList<ValidationError>> Validate()
    {
        var validator = new TournamentSpecValidator(_tournamentRepository, _tournamentInstanceRepository, _applicationConfiguration);
        var errors = await validator.ValidateAsync(this);
        IList<ValidationError> validationErrors = errors.Errors
            .Select(err => new ValidationError(err.PropertyName, err.ErrorMessage))
            .ToList();
        return validationErrors;
    }

    private class TournamentSpecValidator : AbstractValidator<TournamentInstanceSpec>
    {
        private readonly ITournamentRepository _tournamentRepository;
        private readonly ITournamentInstanceRepository _tournamentInstanceRepository;
        private readonly ApplicationConfiguration _applicationConfiguration;

        public TournamentSpecValidator(
            ITournamentRepository tournamentRepository,
            ITournamentInstanceRepository tournamentInstanceRepository,
            ApplicationConfiguration applicationConfiguration
        )
        {
            _tournamentRepository = tournamentRepository;
            _tournamentInstanceRepository = tournamentInstanceRepository;
            _applicationConfiguration = applicationConfiguration;

            RuleFor(s => s.Tournament)
                .NotNull().WithMessage("The tournament must be indicated");
            
            RuleFor(s => s)
                .MustAsync((_, _) => ConcurrentTournamentsLimitIsNotReached())
                .WithMessage("The limit of concurrent tournaments was reached, wait until an ongoing one finishes")
                .WithName(nameof(TournamentInstance));

            RuleFor(s => s.Year)
                .GreaterThanOrEqualTo(2000).WithMessage("Year must be greater than or equal to 2000")
                .LessThanOrEqualTo(3000).WithMessage("Year must be less than or equal to 3000")
                .MustAsync((spec, year, tkn) => OnlyOnePerYear(spec)).WithMessage("The tournament has already been played in the same year");

            RuleFor(s => s.PlayersIds)
                .Configure(cf => cf.PropertyName = "Players")
                .Cascade(CascadeMode.Stop)
                .Must(ps => ps.ToHashSet().Count == ps.Count)
                    .WithMessage("A player cannot be included multiple times")
                .Must((spec, ps) => spec.Tournament is null || spec.Tournament.SinglesDraw == ps.Count)
                    .WithMessage("The number of players must the draw number of the tournament")
                .MustAsync((spec, ids, _) => AllPlayersExist(spec, ids))
                    .WithMessage("Some of the indicated players don't exist")
                .MustAsync((spec, _, _) => PlayersGenderMatchesTournamentGender(spec))
                    .WithMessage("Players gender should match that of the tournament");
        }

        private static async Task<bool> AllPlayersExist(TournamentInstanceSpec tournamentInstanceSpec, IList<Guid> playersIds)
        {
            var players = await tournamentInstanceSpec._players;
            return playersIds.All(id => players.Any(p => p.Id == id));
        }
        
        private static async Task<bool> PlayersGenderMatchesTournamentGender(TournamentInstanceSpec tournamentInstanceSpec)
        {
            if (!tournamentInstanceSpec.Gender.HasValue)
                return true;
            var players = await tournamentInstanceSpec._players;
            return players.All(p => p.Gender == tournamentInstanceSpec.Gender);
        }

        private async Task<bool> ConcurrentTournamentsLimitIsNotReached()
        {
            var ongoingTournaments = await _tournamentInstanceRepository.OngoingTournaments();
            return ongoingTournaments < _applicationConfiguration.MaxConcurrentTournaments;
        }

        private async Task<bool> OnlyOnePerYear(TournamentInstanceSpec tournamentInstanceSpec)
        {
            if (tournamentInstanceSpec.Tournament is null)
                return true;
            var sameYearTournamentExists = await _tournamentInstanceRepository.Exists(
                tournamentInstanceSpec.Tournament.Id,
                tournamentInstanceSpec.Year
            );
            return !sameYearTournamentExists;
        }
    }
}
