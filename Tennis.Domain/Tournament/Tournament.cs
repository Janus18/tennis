using NodaTime;
using Tennis.Domain.Contract;

namespace Tennis.Domain.Tournament;

public class Tournament : IEntity, IAggregate
{
    public Guid Id { get; private set; }
    public TournamentName Name { get; private set; } = new("");
    public Surfaces SurfaceType { get; private set; }
    public string Location { get; private set; } = "";
    public uint SinglesDraw { get; private set; }

    public int StartDateDay { get; private init; }
    public int StartDateMonth { get; private init; }
    public AnnualDate StartDate
    {
        get => new(StartDateMonth, StartDateDay);
        init
        {
            StartDateDay = value.Day;
            StartDateMonth = value.Month;
        }
    }

    public Uri? ImageUri { get; set; }
    
    private Tournament() {}
    
    public Tournament(string name, Surfaces surfaceType, string location, uint singlesDraw, AnnualDate date)
    {
        Name = new TournamentName(name);
        SurfaceType = surfaceType;
        Location = location;
        SinglesDraw = singlesDraw;
        StartDate = date;
    }

    public string? DisplayImageUri => ImageUri?.AbsoluteUri;

    public LocalDate NextPlay
    {
        get
        {
            var now = DateTime.UtcNow;
            var sd = StartDate.InYear(now.Year); 
            return sd <= LocalDate.FromDateTime(now) ? sd.Plus(Period.FromYears(1)) : sd;
        }
    }
}