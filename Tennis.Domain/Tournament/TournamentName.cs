using Tennis.Domain.ValueObjects;

namespace Tennis.Domain.Tournament;

public record TournamentName : NormalizedName
{
    public TournamentName(string value) : base(value)
    {
    }
}

