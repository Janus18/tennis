using NodaTime;

namespace Tennis.Domain.Tournament.Generators.Contract;

public interface IMatchCreationStrategy
{
    IEnumerable<Match.Match> CreateMatches(LocalDate startDate);
}
