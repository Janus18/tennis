using NodaTime;
using Tennis.Domain.Configuration;
using Tennis.Domain.Exceptions;

namespace Tennis.Domain.Tournament.Generators.Implementation;

public class MatchDateProvider
{
    private LocalDate _currentDate;
    private uint _currentDateMatchesCount = 0;
    private readonly ushort _maxMatchesPerDay;

    public MatchDateProvider(LocalDate start, ApplicationConfiguration configuration)
    {
        if (configuration.MaxMatchesPerDay < 1)
            throw new DomainException($"{nameof(configuration.MaxMatchesPerDay)} must be greater or equal to 1");
        _currentDate = start;
        _maxMatchesPerDay = configuration.MaxMatchesPerDay;
    }

    public LocalDate Next()
    {
        if (_currentDateMatchesCount >= _maxMatchesPerDay)
        {
            _currentDate = _currentDate.With(d => d.PlusDays(1));
            _currentDateMatchesCount = 1;
        }
        else
        {
            _currentDateMatchesCount++;
        }
        return _currentDate;
    }
}