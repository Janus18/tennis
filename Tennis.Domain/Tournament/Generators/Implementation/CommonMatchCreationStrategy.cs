using NodaTime;
using Tennis.Domain.Configuration;
using Tennis.Domain.Extensions;
using Tennis.Domain.Tournament.Generators.Contract;

namespace Tennis.Domain.Tournament.Generators.Implementation;

public class CommonMatchCreationStrategy : IMatchCreationStrategy
{
    private readonly ICollection<Player.Player> _players;
    private readonly ApplicationConfiguration _configuration;

    public CommonMatchCreationStrategy(ICollection<Player.Player> players, ApplicationConfiguration configuration)
    {
        if (!((uint)players.Count).IsPowerOfTwo())
            throw new ArgumentException("Number of players must be a power of 2.", nameof(players));
        _players = players;
        _configuration = configuration;
    }

    public IEnumerable<Match.Match> CreateMatches(LocalDate startDate)
    {
        var dateProvider = new MatchDateProvider(startDate, _configuration);
        var firstRound = CreateFirstRoundMatches(dateProvider, _players);
        var otherMatches = Enumerable.Range(2, RoundsCount(_players.Count))
            .SelectMany(r => CreateEmptyRoundMatches(dateProvider, _players.Count, r)).ToList();
        return firstRound.Concat(otherMatches).ToList();
    }

    private static IEnumerable<Match.Match> CreateFirstRoundMatches(MatchDateProvider matchDateProvider, ICollection<Player.Player> players)
        => Enumerable.Range(0, players.Count / 2)
            .Select(x => (Player1: players.ElementAt(x * 2), Player2: players.ElementAt(x * 2 + 1)))
            .Select((ps, i) => new Match.Match(ps.Player1, ps.Player2, matchDateProvider.Next(), 1, i + 1))
            .ToList();

    private static IEnumerable<Match.Match> CreateEmptyRoundMatches(MatchDateProvider matchDateProvider, int playersCount, int round)
        => Enumerable.Range(1, RoundMatchesCount(playersCount, round))
            .Select(match => new Match.Match(matchDateProvider.Next(), round, match)).ToList();

    private static int RoundsCount(int playersCount) => (int)Math.Log2(playersCount);
    private static int RoundMatchesCount(int playersCount, int round) => (int)(playersCount / Math.Pow(2, round));
}