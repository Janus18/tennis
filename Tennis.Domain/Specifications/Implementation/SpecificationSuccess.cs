using System.Collections.ObjectModel;
using Tennis.Domain.Specifications.Contract;

namespace Tennis.Domain.Specifications.Implementation;

public class SpecificationSuccess<TValue> : ISpecificationResult<TValue>
{
    private TValue Value { get; }

    public SpecificationSuccess(TValue value)
    {
        Value = value;
    }

    public T Process<T>(System.Func<TValue, T> onSuccess, System.Func<IList<ValidationError>, T> onError)
        => onSuccess(Value);

    public void Process(Action<TValue> onSuccess, Action<IList<ValidationError>> onError)
        => onSuccess(Value);

    public TValue Unpack()
        => Value;

    public IList<ValidationError> Failures()
        => new Collection<ValidationError>();
}
