using System.Text.Json;

namespace Tennis.Domain.Specifications.Implementation;

public class SpecificationException : ApplicationException
{
    public IList<ValidationError> Errors { get; }

    public SpecificationException(IList<ValidationError> errors)
    {
        Errors = errors;
    }

    public override string Message =>
        JsonSerializer.Serialize(Errors, new JsonSerializerOptions { WriteIndented = true });
}