using FluentValidation.Results;

namespace Tennis.Domain.Specifications.Implementation;

public class ValidationError
{
    public string Code { get; }
    public string Description { get; }

    public ValidationError(string code, string description)
    {
        Code = code;
        Description = description;
    }
    
    public ValidationError(ValidationFailure failure)
    {
        Code = failure.PropertyName;
        Description = failure.ErrorMessage;
    }
}