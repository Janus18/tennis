using Tennis.Domain.Specifications.Contract;

namespace Tennis.Domain.Specifications.Implementation;

public class SpecificationError<TValue> : ISpecificationResult<TValue>
{
    private IList<ValidationError> Errors { get; }

    public SpecificationError(IList<ValidationError> errors)
    {
        Errors = errors;
    }

    public T Process<T>(System.Func<TValue, T> onSuccess, System.Func<IList<ValidationError>, T> onError)
        => onError(Errors);

    public void Process(Action<TValue> onSuccess, Action<IList<ValidationError>> onError)
        => onError(Errors);

    public TValue Unpack()
        => throw new SpecificationException(Errors);

    public IList<ValidationError> Failures()
        => Errors;
}
