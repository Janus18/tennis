using Tennis.Domain.Specifications.Implementation;

namespace Tennis.Domain.Specifications.Contract;

public interface IAsyncSpecification<TValue>
{
    Task<ISpecificationResult<TValue>> Apply();

    Task<IList<ValidationError>> Validate();
}