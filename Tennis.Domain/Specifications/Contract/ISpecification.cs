using Tennis.Domain.Specifications.Implementation;

namespace Tennis.Domain.Specifications.Contract;

public interface ISpecification<out TValue>
{
    ISpecificationResult<TValue> Apply();

    IList<ValidationError> Validate();
}