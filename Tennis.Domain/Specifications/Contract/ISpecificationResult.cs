using Tennis.Domain.Specifications.Implementation;

namespace Tennis.Domain.Specifications.Contract;

public interface ISpecificationResult<out TValue>
{
    T Process<T>(Func<TValue, T> onSuccess, Func<IList<ValidationError>, T> onError);

    void Process(Action<TValue> onSuccess, Action<IList<ValidationError>> onError);

    TValue Unpack();

    IList<ValidationError> Failures();
}