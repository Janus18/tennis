using Tennis.Domain.ValueObjects;

namespace Tennis.Domain.Player;

public record PlayerName : NormalizedName
{
    public PlayerName(string value) : base(value)
    {
    }
}

