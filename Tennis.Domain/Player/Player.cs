﻿using Tennis.Domain.Contract;
using Tennis.Domain.ValueObjects;

namespace Tennis.Domain.Player;

public class Player : IEntity, IAggregate
{
    public Guid Id { get; private set; }

    public PlayerName Name { get; private set; } = new("");

    public AbilityPoints Strength { get; private set; } = AbilityPoints.Zero;

    public AbilityPoints Speed { get; private set; } = AbilityPoints.Zero;

    public AbilityPoints Reaction { get; private set; } = AbilityPoints.Zero;

    public Gender Gender { get; private set; }
    
    public Uri? ImageUri { get; set; }

    private Player() { }

    public Player(
        string name,
        AbilityPoints strength,
        AbilityPoints speed,
        AbilityPoints reaction,
        Gender gender)
    {
        Name = new PlayerName(name);
        Strength = strength;
        Speed = speed;
        Reaction = reaction;
        Gender = gender;
    }

    public WeightedValue<Player> Weighted() => new(this, Strength.Value + Speed.Value + Reaction.Value);

    public string? ImageUriAbsolute => ImageUri?.AbsoluteUri;
}
