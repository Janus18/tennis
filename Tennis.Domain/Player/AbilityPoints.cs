using Tennis.Domain.Specifications.Contract;
using Tennis.Domain.Specifications.Implementation;

namespace Tennis.Domain.Player;

public class AbilityPoints
{
    public static AbilityPoints Zero = new(0); 
    
    public int Value { get; }

    private AbilityPoints(int value)
    {
        Value = value;
    }

    public static ISpecificationResult<AbilityPoints> TryCreate(int value)
    {
        if (value is < 0 or > 100)
        {
            return new SpecificationError<AbilityPoints>(
                new[] { new ValidationError(nameof(Value), "Ability points must be between 0 and 100") }
            );
        }

        return new SpecificationSuccess<AbilityPoints>(new AbilityPoints(value));
    }

    public static AbilityPoints Create(int value) => TryCreate(value).Unpack();

    public override bool Equals(object? obj) => obj is AbilityPoints other && other.Value == Value;

    public override int GetHashCode() => Value.GetHashCode();
}