namespace Tennis.Domain.Player;

public enum Gender
{
    Male = 1,
    Female = 2
}
