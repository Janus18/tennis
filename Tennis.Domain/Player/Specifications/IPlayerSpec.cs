using Tennis.Domain.Specifications.Contract;

namespace Tennis.Domain.Player.Specifications;

public interface IPlayerSpec : IAsyncSpecification<Player>
{
    Gender Gender { get; }

    string? Name { get; }

    int Reaction { get; }

    int Speed { get; }

    int Strength { get; }
}