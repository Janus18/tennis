using System.Text.RegularExpressions;
using FluentValidation;
using Microsoft.Extensions.Options;
using Tennis.Domain.Configuration;
using Tennis.Domain.Contract.DataAccess;
using Tennis.Domain.Specifications.Contract;
using Tennis.Domain.Specifications.Implementation;
using Tennis.Domain.ValueObjects;

namespace Tennis.Domain.Player.Specifications;

public class PlayerSpec : IPlayerSpec
{
    private readonly IPlayerRepository _playerRepository;
    private readonly ApplicationConfiguration _applicationConfiguration;

    public Gender Gender { get; init; }

    public string Name { get; init; } = "";

    public int Reaction { get; init; }

    public int Speed { get; init; }
    
    public int Strength { get; init; }

    public PlayerSpec(
        IPlayerRepository playerRepository,
        IOptions<ApplicationConfiguration> options
    )
    {
        _playerRepository = playerRepository;
        _applicationConfiguration = options.Value;
    }

    public async Task<ISpecificationResult<Player>> Apply()
    {
        var validationErrors = await Validate();
        if (validationErrors.Any())
            return new SpecificationError<Player>(validationErrors);
        return new SpecificationSuccess<Player>(new Player(
            Name!,
            AbilityPoints.Create(Strength),
            AbilityPoints.Create(Speed),
            AbilityPoints.Create(Reaction),
            Gender));
    }

    public async Task<IList<ValidationError>> Validate()
    {
        var validator = new PlayerSpecValidator(_playerRepository, _applicationConfiguration);
        var errors = await validator.ValidateAsync(this);
        IList<ValidationError> validationErrors =
            errors.Errors
                .Select(err => new ValidationError(err.PropertyName, err.ErrorMessage))
                .ToList();
        return validationErrors;
    }

    private class PlayerSpecValidator : AbstractValidator<PlayerSpec>
    {
        private readonly IPlayerRepository _playerRepository;
        private readonly ApplicationConfiguration _applicationConfiguration;

        public PlayerSpecValidator(
            IPlayerRepository playerRepository,
            ApplicationConfiguration applicationConfiguration
        )
        {
            _playerRepository = playerRepository;
            _applicationConfiguration = applicationConfiguration;

            RuleFor(s => s)
                .MustAsync((_, _) => PlayersCountIsNotExceeded())
                .WithMessage("No new players can be added, the limit was reached")
                .WithName(nameof(Player));

            RuleFor(s => s.Gender)
                .Must(g => Enum.IsDefined(typeof(Gender), g)).WithMessage("The gender must be one of the available options");

            RuleFor(s => s.Name)
                .Cascade(CascadeMode.Stop)
                .NotEmpty().WithMessage("Name must be a non-empty string")
                .Must(name => NormalizedName.IsValid(name))
                    .WithMessage("The name must consist of letters, digits and spaces only")
                .MustAsync((name, tkn) => NameIsUnique(name!))
                    .WithMessage("A player with the same name already exists");

            RuleFor(s => s.Reaction)
                .Custom((r, ctx) =>
                    AbilityPoints.TryCreate(r).Failures().ToList().ForEach(x => ctx.AddFailure($"{ctx.PropertyName}.{x.Code}", x.Description))
                );

            RuleFor(s => s.Speed)
                .Custom((r, ctx) =>
                    AbilityPoints.TryCreate(r).Failures().ToList().ForEach(x => ctx.AddFailure($"{ctx.PropertyName}.{x.Code}", x.Description))
                );

            RuleFor(s => s.Strength)
                .Custom((r, ctx) =>
                    AbilityPoints.TryCreate(r).Failures().ToList().ForEach(x => ctx.AddFailure($"{ctx.PropertyName}.{x.Code}", x.Description))
                );
        }

        private async Task<bool> NameIsUnique(string name)
        {
            var homonymousPlayerExists = await _playerRepository.Exists(p => p.Name == new PlayerName(name));
            return !homonymousPlayerExists;
        }

        private async Task<bool> PlayersCountIsNotExceeded()
        {
            var currentCount = await _playerRepository.Count(_ => true);
            return currentCount < _applicationConfiguration.MaxPlayersCount;
        }
    }
}