namespace Tennis.Domain.Contract;

public interface IEntity
{
    Guid Id { get; }
}
