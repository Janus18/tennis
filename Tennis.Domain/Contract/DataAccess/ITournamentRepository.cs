namespace Tennis.Domain.Contract.DataAccess;

public interface ITournamentRepository : IRepository<Tournament.Tournament>
{
}
