using System.Linq.Expressions;

namespace Tennis.Domain.Contract.DataAccess;

public interface IOrder<T>
{
    IOrderedQueryable<T> Order(IQueryable<T> query);

    IList<IOrderBy<T>> Collect(IList<IOrderBy<T>> orders);

    IOrder<T> ThenBy<T2>(Expression<Func<T, T2>> exp);
    
    IOrder<T> ThenByDesc<T2>(Expression<Func<T, T2>> exp);
}
