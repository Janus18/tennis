namespace Tennis.Domain.Contract.DataAccess;

public interface IOrderBy<T>
{
    IOrderedQueryable<T> By(IQueryable<T> query);
    
    IOrderedQueryable<T> ThenBy(IOrderedQueryable<T> query);
}
