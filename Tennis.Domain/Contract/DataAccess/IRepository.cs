using System.Linq.Expressions;
using Tennis.Domain.ValueObjects;

namespace Tennis.Domain.Contract.DataAccess;

public interface IRepository<TAggregate> where TAggregate : IAggregate
{
    void Add(TAggregate aggregate);
    
    /// <summary>
    /// Shortcut to Add followed by SaveChanges.
    /// </summary>
    /// <param name="aggregate"></param>
    /// <returns>Task</returns>
    Task AddAsync(TAggregate aggregate);

    void Update(TAggregate aggregate);

    /// <summary>
    /// Shortcut to Update followed by SaveChanges.
    /// </summary>
    /// <param name="aggregate"></param>
    /// <returns>Task</returns>
    Task UpdateAsync(TAggregate aggregate);

    void Delete(TAggregate aggregate);

    /// <summary>
    /// Shortcut to Delete followed by SaveChanges.
    /// </summary>
    /// <param name="aggregate"></param>
    /// <returns>Task</returns>
    Task DeleteAsync(TAggregate aggregate);

    Task<TAggregate?> Get(
        Expression<Func<TAggregate, bool>> predicate,
        IEnumerable<Expression<Func<TAggregate, object>>>? include = null
    );

    Task<IEnumerable<TAggregate>> GetMany(
        Expression<Func<TAggregate, bool>> predicate,
        IEnumerable<Expression<Func<TAggregate, object>>>? include = null,
        IOrder<TAggregate>? order = null,
        Page? page = null
    );
    
    Task<bool> Exists(Expression<Func<TAggregate, bool>> predicate);

    Task<uint> Count(Expression<Func<TAggregate, bool>> predicate);

    Task SaveChanges();
}
