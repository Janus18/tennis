using Tennis.Domain.Tournament;

namespace Tennis.Domain.Contract.DataAccess;

public interface ITournamentInstanceRepository : IRepository<TournamentInstance>
{
    Task<Tournament.TournamentInstance?> Get(Guid tournamentInstanceId);

    Task<bool> Exists(Guid tournamentId, int year);
    
    Task<int> OngoingTournaments();
}
