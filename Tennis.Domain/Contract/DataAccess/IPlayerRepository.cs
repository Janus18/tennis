namespace Tennis.Domain.Contract.DataAccess;

public interface IPlayerRepository : IRepository<Player.Player>
{
}
