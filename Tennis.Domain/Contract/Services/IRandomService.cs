using Tennis.Domain.ValueObjects;

namespace Tennis.Domain.Contract.Services;

public interface IRandomService
{
    T Choose<T>(IList<WeightedValue<T>> values);
}