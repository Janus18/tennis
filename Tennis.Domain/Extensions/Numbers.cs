namespace Tennis.Domain.Extensions;

public static class Numbers
{
    public static bool IsPowerOfTwo(this uint x)  => x != 0 && (x & (x - 1)) == 0;
}