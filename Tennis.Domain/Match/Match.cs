using NodaTime;
using Tennis.Domain.Contract;
using Tennis.Domain.Contract.Services;
using Tennis.Domain.Exceptions;
using Tennis.Domain.ValueObjects;

namespace Tennis.Domain.Match;

public class Match : IEntity
{
    public Guid Id { get; protected set; }
    
    public LocalDate Date { get; private set; }
    
    public int Round { get; private set; }
    
    public int MatchNumber { get; private set; }

    private Player.Player? _player1;
    public Player.Player? Player1
    {
        get => _player1;
        private set => _player1 = Player1 is null
            ? value
            : throw new DomainException($"Player1 was added already. Match ID {Id}");
    }

    private Player.Player? _player2;
    public Player.Player? Player2
    {
        get => _player2;
        private set => _player2 = _player2 is null
            ? value
            : throw new DomainException($"Player2 was added already. Match ID {Id}");
    }

    public Player.Player? Winner { get; private set; }

    private Match() { }

    public Match(Player.Player player1, Player.Player player2, LocalDate date, int round, int matchNumber)
    {
        Date = date;
        MatchNumber = matchNumber;
        Player1 = player1;
        Player2 = player2;
        Round = round;
    }

    public Match(LocalDate date, int round, int matchNumber)
    {
        Date = date;
        MatchNumber = matchNumber;
        Round = round;
    }

    public bool IsFinished => Winner != null;
    
    public int NextRound => Round + 1;

    public bool PlayersAreReady => Player1 is not null && Player2 is not null; 

    public Player.Player Play(IRandomService randomService)
    {
        if (!PlayersAreReady)
            throw new DomainException($"Players must be defined to play the match. ID {Id}");
        if (Winner is not null)
            return Winner;
        var weightedValues = new List<WeightedValue<Player.Player>>
        {
            Player1!.Weighted(),
            Player2!.Weighted()
        };
        Winner = randomService.Choose(weightedValues);
        return Winner;
    }

    public void AddPlayer(Match previousMatch)
    {
        if (previousMatch.IsFirstMatch)
            Player1 = previousMatch.Winner;
        else
            Player2 = previousMatch.Winner;
    }

    /// <summary>
    /// Does this match determines the first player of the next match?
    /// </summary>
    private bool IsFirstMatch => MatchNumber % 2 == 1;
}
